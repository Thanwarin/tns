const functions = require("firebase-functions");
const admin = require('firebase-admin');
const nodemailer = require('nodemailer')
admin.initializeApp();
require('dotenv').config();
const {SENDER_EMAIL,SENDER_PASSWORD}=process.env;
const db = admin.firestore();

exports.sendEmailNotificationCheckIn=functions.firestore.document('messages/{docId}') //checkIn type2
.onCreate((snap,ctx)=>{
    const data =snap.data();
    let authData=nodemailer.createTransport({
        host:'smtp.gmail.com',
        port:465,
        secure:true,
        auth:{
            user:SENDER_EMAIL,
            pass:SENDER_PASSWORD
        }
    });
    if(data.type == 2){
        authData.sendMail({
            from:'tns.cpe.rmutt@gmail.com',
            to:`${data.receiver?.email}`,
            subject:'ระบบตรวจสอบและแจ้งเตือนการรับพัสดุไปรษณีย์',
            text:`พัสดุหมายเลข ${data.data?.trackNumber} มาถึงแล้ว !!!`,
            html:`พัสดุหมายเลข ${data.data?.trackNumber} มาถึงแล้ว !!!`
        }).then(res=>console.log('success')).catch(err=>console.log(err));
    
    }
})

exports.sendEmailNotificationCheckOut=functions.firestore.document('messages/{docId}')//checkOut type0
.onCreate((snap,ctx)=>{
    const data = snap.data();
    let authData=nodemailer.createTransport({
        host:'smtp.gmail.com',
        port:465,
        secure:true,
        auth:{
            user:SENDER_EMAIL,
            pass:SENDER_PASSWORD
        }
    });
    if(data.type == 0){
        authData.sendMail({
            from:'tns.cpe.rmutt@gmail.com',
            to:`${data.receiver?.email}`,
            subject:'ระบบตรวจสอบและแจ้งเตือนการรับพัสดุไปรษณีย์',
            text:`พัสดุหมายเลข ${data.data?.trackNumber} ได้รับเรียบร้อยแล้ว
            เจ้าของพัสดุ : ${data.data?.owner?.firstname} ${data.data?.owner?.lastname}
            ผู้รับพัสดุ : ${data.data?.receiver_parcel?.firstname} ${data.data?.receiver_parcel?.lastname}`,
            html:`พัสดุหมายเลข ${data.data?.trackNumber} ได้รับเรียบร้อยแล้ว
            เจ้าของพัสดุ : ${data.data?.owner?.firstname} ${data.data?.owner?.lastname}
            ผู้รับพัสดุ : ${data.data?.receiver_parcel?.firstname} ${data.data?.receiver_parcel?.lastname}`
        }).then(res=>console.log('success')).catch(err=>console.log(err));
    }
})
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
