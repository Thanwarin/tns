import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/services/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  @Output() gotoPage = new EventEmitter<string>();
  constructor(private authservice: AuthService, public auth: AngularFireAuth, private authService: AuthService, private cookieService: CookieService) {

  }
  hide: boolean = true;
  email: string = '';
  password: string = '';
  c_email: string = '';
  c_password = {};
  remember!: boolean;
  cookie: object[] = [];

  ngOnInit(): void {
    const cookie: {} = this.cookieService.getAll();
    this.cookie.push(cookie);
    this.cookie.forEach(item => {
    })
    const login = document.getElementById('pass');
    login?.addEventListener('keyup', function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById('login')?.click();
      }
    })
  }

  goTo(signUp: string) {
    this.gotoPage.emit(signUp);
  }

  remember_me(e: any) {
    if (e.target.checked === true) {
      this.remember = true;
    } else {
      this.remember = false;
    }
  }

  loginGg() {
    this.authService.googleLogin();
  }
  loginAuth_Email() {
    this.authService.singIn(this.email, this.password, this.remember);
  }
  resetPassword() {
    if (this.email !== '') {
      this.authservice.resetPassword(this.email);
    } else {
      alert('กรุณากรอกที่อยู่อีเมล');
    }
  }
}