import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  page: string = 'signIn';
  emailToken:string='';
  users$: Observable<any[]>;
  password:string='';
  constructor(private auth: AuthService, private aft: AngularFirestore) {
    this.users$ = aft.collection('users').valueChanges({idField:'ID'});
   }

  ngOnInit(): void {
  }
  goTo(event: string) {
    this.page = event;
  }
}
