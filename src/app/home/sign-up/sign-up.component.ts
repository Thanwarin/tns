import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AbstractControl, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { number } from 'echarts';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  @Output() gotoPage = new EventEmitter<string>();
  firstname: string = '';
  lastname: string = '';
  email: string = '';
  department: string = '';
  imageProfile: string = '';
  phoneNumber!: number;
  password: string = '';
  check_password = '';
  crate = null;
  key_code:string='';
  department_En = [
    'เจ้าหน้าที่',
    'วิศวกรรมโยธา', 'วิศวกรรมวัสดุและโลหะการ', 'วิศวกรรมไฟฟ้า',
    'วิศวกรรมเครื่องกล', 'วิศวกรรมอุตสาหการ', 'วิศวกรรมสิ่งทอ',
    'วิศวกรรมอิเล็กทรอนิกส์และโทรคมนาคม', 'วิศวกรรมคอมพิวเตอร์',
    'วิศวกรรมเคมี', 'วิศวกรรมเกษตร'
  ];
  rq_fname = new FormControl('', [
    Validators.required
  ])
  rq_lname = new FormControl('', [
    Validators.required
  ])
  rq_department = new FormControl('', [
    Validators.required,
  ])
  rq_email = new FormControl('', [
    Validators.required,
    Validators.email
  ])
  pass = new FormControl('', [
    Validators.required,
    Validators.pattern("^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-6]).{8,255})\\S$")
  ]);
  confirmPassword = new FormControl('', [
    Validators.required,
    this.confirmEquals()
  ])
  check_keyCode = new FormControl('',[
    Validators.required
  ])
  constructor(private angularFirestore: AngularFirestore, private auth: AuthService) { }
  ngOnInit(): void {

  }
  goTo(signIn: string) {
    this.gotoPage.emit(signIn);
  }
  confirmEquals(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null =>
      control.value?.toLowerCase() === this.passwordValue.toLowerCase() ? null : { noMatch: true };
  }
  get passwordValue() {
    return this.pass.value;
  }
  fnameMessage() {
    return this.rq_fname.hasError('required') ? 'โปรดกรอกชื่อ' : '';
  }
  lnameMessage() {
    return this.rq_lname.hasError('required') ? 'โปรดกรอกนามสกุล' : '';
  }
  department_Message() {
    return this.rq_lname.hasError('required') ? 'โปรดเลือกภาควิชา หรือ หน่วยงานที่สังกัด' : '';
  }
  emailMessage() {
    if (this.rq_email.hasError('required')) {
      return 'โปรดกรอกที่อยู่อีเมล';
    }
    return this.rq_email.hasError('email') ? 'ที่อยู่อีเมลไม่ถูกต้อง' : '';
  }
  keyCode_Message(){
    return this.check_keyCode.hasError('required')? 'โปรดกรอกรหัสสำหรับลงทะเบียน' : '';
  }

  signUp() {
    if (this.firstname !== '' && this.lastname !== '' && this.department !== '' && this.email !== '' && this.password !== '' && this.check_password !== '' && this.key_code !== '') {
      if (this.check_password === this.password) {
        this.auth.emailSignup(
          this.email,
          this.password,
          this.department,
          this.firstname,
          this.lastname,
          this.key_code
          );
      } else {
        alert('รหัสผ่านไม่ตรงกัน');
      }
    }else{
      alert('กรุณากรอกข้อมูลให้ครบถ้วน');
      
    }
  }
}
