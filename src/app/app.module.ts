import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxEchartsModule } from 'ngx-echarts';

/*AngularFire*/
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorage } from '@angular/fire/storage/';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

/*Service*/
import { MessagingService } from './services/messaging.service';
import { CookieService } from 'ngx-cookie-service';

/*Angular mat*/
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBadgeModule } from '@angular/material/badge';



/*Component*/
import { AppComponent } from './app.component';
import { SignInComponent } from './home/sign-in/sign-in.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { environment } from '../environments/environment';
import { SignUpComponent } from './home/sign-up/sign-up.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AdminDashboardStatisticsComponent } from './admin/admin-dashboard-statistics/admin-dashboard-statistics.component';
import { AdminHistoryComponent } from './admin/admin-history/admin-history.component';
import { AdminHomePageComponent, AdminPopUpRandomComponent } from './admin/admin-home-page/admin-home-page.component';
import { AdminEditParcelComponent, AdminPopUpCheckOutComponent, CardComponent, PopUpAdminAddTrackingSuccessComponent, PopUpAdminCheckOutParcelComponent, PopUpAdminRemoveParcelComponent } from './admin/admin-home-page/card/card.component';
import { AdminListEditParcelComponent, AdminListPopUpCheckOutComponent, ListComponent, PopUpAdminAddListTrackingSuccessComponent, PopUpAdminListCheckOutParcelComponent, PopUpAdminListRemoveParcelComponent } from './admin/admin-home-page/list/list.component';
import { AdminMenuBarComponent } from './admin/admin-menu-bar/admin-menu-bar.component';
import { AdminNotificationComponent } from './admin/admin-notification/admin-notification.component';
import { AdminScanComponent, AddImageComponent } from './admin/admin-scan/admin-scan.component';
import { AdminComponent } from './admin/admin.component';
import { AddReceiverComponent, PopupAddReceiverComponent, PopUpRemoveReceiverComponent, PopUpWaitReceiverComponent } from './user/add-receiver/add-receiver.component';
import { DashboardStatisticsComponent } from './user/dashboard-statistics/dashboard-statistics.component';
import { EditProfileComponent, PopupResetPasswordComponent, ResetPasswordComponent } from './user/edit-profile/edit-profile.component';
import { CardHistoryComponent } from './user/history/card-history/card-history.component';
import { HistoryComponent } from './user/history/history.component';
import { AssignReceiveParcelComponent, CardParcelsComponent, EditParcelComponent, PopUpRemoveParcelComponent } from './user/home-page/card-parcels/card-parcels.component';
import { HomePageComponent } from './user/home-page/home-page.component';
import { AssignReceiveParcelListComponent, EditParcelListComponent, ListParcelsComponent, PopUpAddTrackingSuccessListComponent, PopUpRemoveParcelListComponent } from './user/home-page/list-parcels/list-parcels.component';
import { MenuBarComponent } from './user/menu-bar/menu-bar.component';
import { NotificationComponent } from './user/notification/notification.component';
import { ScanComponent } from './user/scan/scan.component';
import { UserComponent } from './user/user.component';
import { ListHistoryComponent } from './user/history/list-history/list-history.component';
import { AdminCardHistoryComponent } from './admin/admin-history/admin-card-history/admin-card-history.component';
import { AdminListHistoryComponent } from './admin/admin-history/admin-list-history/admin-list-history.component';
import { PopupNotifyAgentComponent, TypeNotificationComponent } from './user/notification/type-notification/type-notification.component';
import { AdminTypeNotificationComponent } from './admin/admin-notification/admin-type-notification/admin-type-notification.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignInComponent,
    SignUpComponent,

    UserComponent,
    CardParcelsComponent,
    MenuBarComponent,
    ScanComponent,
    HomePageComponent,
    EditProfileComponent,
    HistoryComponent,
    ListParcelsComponent,
    CardHistoryComponent,
    NotificationComponent,
    DashboardStatisticsComponent,
    AddReceiverComponent,
    EditParcelComponent,
    PopUpRemoveParcelComponent,
    AssignReceiveParcelComponent,
    EditParcelListComponent,
    PopUpAddTrackingSuccessListComponent,
    PopUpRemoveParcelListComponent,
    AssignReceiveParcelListComponent,
    ResetPasswordComponent,
    PopupResetPasswordComponent,
    ListHistoryComponent,
    PopUpWaitReceiverComponent,
    PopupAddReceiverComponent,
    PopUpRemoveReceiverComponent,
    TypeNotificationComponent,
    PopupNotifyAgentComponent,

    AdminComponent,
    AdminHomePageComponent,
    AdminHistoryComponent,
    AdminScanComponent,
    AdminNotificationComponent,
    AdminDashboardStatisticsComponent,
    AdminMenuBarComponent,
    CardComponent,
    ListComponent,
    AddImageComponent,
    ListComponent,
    AdminCardHistoryComponent,
    AdminListHistoryComponent,
    AdminEditParcelComponent,
    PopUpAdminAddTrackingSuccessComponent,
    PopUpAdminRemoveParcelComponent,
    AdminPopUpCheckOutComponent,
    PopUpAdminCheckOutParcelComponent,
    PopUpAdminListCheckOutParcelComponent,
    PopUpAdminListRemoveParcelComponent,
    PopUpAdminAddListTrackingSuccessComponent,
    AdminListPopUpCheckOutComponent,
    AdminListEditParcelComponent,
    AdminTypeNotificationComponent,
    AdminPopUpRandomComponent
  ],
  imports: [
    FormsModule,
    // FormGroup,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatDialogModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSnackBarModule,
    CdkTableModule,
    MatIconModule,
    MatExpansionModule,
    MatTooltipModule,
    MatToolbarModule,
    MatMenuModule,
    MatSelectModule,
    MatBadgeModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    ScrollingModule,
    MatSlideToggleModule,
    ZXingScannerModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireMessagingModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
    NgbModule,

  ],
  providers: [AngularFireStorage,
    MessagingService,
    CookieService,
    AuthService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
