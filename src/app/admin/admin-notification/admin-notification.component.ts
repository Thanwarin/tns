import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireMessaging } from '@angular/fire/messaging';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { MessagesModel } from 'src/app/@common/model/message';
import { UsersModel } from 'src/app/@common/model/users';
import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-admin-notification',
  templateUrl: './admin-notification.component.html',
  styleUrls: ['./admin-notification.component.scss']
})
export class AdminNotificationComponent implements OnInit {
  messages$: Observable<any[]>;
  token: any;
  message:any[]=[];
  users$:Observable<any[]>;
  data_user:UsersModel[]=[];
  data:any[]=[];
  firstname:string='';
  lastname:string='';
  messages:MessagesModel[]=[];
  count_messages:number= 0;
  constructor(private afMessaging: AngularFireMessaging, private mgs: MessagingService, private afs: AngularFirestore) {
    this.messages$ = afs.collection('messages',ref=>ref.orderBy('dateTime','desc')).valueChanges({idField:'ID'});
    this.users$ = afs.collection('users').valueChanges();
    this.messages$.subscribe((data)=>{
    })
  }
  ngOnInit(): void {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    const email = token.forEach((token) => {
      this.users$.subscribe((data)=>{
        data.forEach((user)=>{
          if(user.email === token.email){
            this.data_user.push(user)
            this.firstname = user.firstname;
            this.lastname = user.lastname;
          }
        })
      })
    });
    this.messages$.subscribe((data)=>{
      this.messages = [];
      data.forEach((messages:any)=>{
        if(messages.receiver?.firstname === this.firstname && messages.receiver.lastname === this.lastname){
              this.messages.push(messages);
              this.count_messages =  this.messages.length;
        }
      })
    });
  }

}
