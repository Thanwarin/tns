import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { UsersModel } from 'src/app/@common/model/users';

@Component({
  selector: 'app-admin-type-notification',
  templateUrl: './admin-type-notification.component.html',
  styleUrls: ['./admin-type-notification.component.scss']
})
export class AdminTypeNotificationComponent implements OnInit {
  @Input() messages:any={};
  @Input() firstname:string='';
  @Input() lastname:string='';
  id_message:string='';
  dateTime ='';
  sender_fname:string='';
  sender_lname:string='';
  path_Profile:string='';
  data_owner:UsersModel={};
  id_owner:string='';
  constructor(private angularstorage: AngularFireStorage,private aft : AngularFirestore) { }

  ngOnInit(): void {
    const dateTime = new Date(`${this.messages.dateTime}`);
    const result_dateTime = dateTime.toLocaleTimeString('th-TH',{
      year:'numeric',
      month:'long',
      day:'numeric',
      hour:'2-digit',
      minute:'numeric',
    });
    this.id_message = this.messages.ID;
    this.dateTime = result_dateTime;
  }

}
