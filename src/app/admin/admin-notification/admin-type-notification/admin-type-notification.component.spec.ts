import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTypeNotificationComponent } from './admin-type-notification.component';

describe('AdminTypeNotificationComponent', () => {
  let component: AdminTypeNotificationComponent;
  let fixture: ComponentFixture<AdminTypeNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTypeNotificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTypeNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
