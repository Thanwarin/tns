import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import jwtDecode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin-home-page',
  templateUrl: './admin-home-page.component.html',
  styleUrls: ['./admin-home-page.component.scss'],
})
export class AdminHomePageComponent implements OnInit {
  listWaitParcels$: Observable<any[]>;
  statusPage: number = 0;
  statusParcels: boolean = false;
  list_Parcel0: any[] = [];
  list_Parcel1: any[] = [];
  name = '';
  receiverName = '';
  track = '';
  date = '';
  name_Filter: string = '';
  receiver_Filter: string = '';
  track_Filter: string = '';
  date_Filter: string = '';
  owner: object[] = [];
  receiver: any[] = [];
  trackNumber: object[] = [];
  timeStamp: string[] = [];
  setDate = '';
  deCodeName = '';
  users$: Observable<any[]>;
  search_Name: EventEmitter<string> = new EventEmitter();
  search_Track: EventEmitter<string> = new EventEmitter();
  search_Date: EventEmitter<string> = new EventEmitter();
  stateSearch: number = 0;
  selectSearch: string = 'เจ้าของพัสดุ';
  result: string = '';
  id: any[] = [];
  constructor(
    public modalService: NgbModal,
    private firestore: AngularFirestore,
    private cookieService: CookieService,
    configModal: NgbModalConfig
  ) {
    this.listWaitParcels$ = firestore
      .collection('parcels', (ref) => ref.orderBy('checkIn', 'desc'))
      .valueChanges({ idField: 'ID' });
    this.users$ = firestore.collection('users').valueChanges({ idField: 'ID' });
    this.users$.subscribe(data => {
      data.forEach(users => {
        if (users.role === 'admin') {
          this.id.push(users.ID);
        }
      })
    })
    configModal.backdrop = 'static';
    configModal.keyboard = false;
  }

  ngOnInit(): void { }
  ngAfterViewInit() {
    setTimeout(() => {
      this.search();
    }, 300);
  }
  open_popup_randomKey() {
    let result = '';
    const char =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charLength = char.length;
    for (let i = 0; i < 6; i++) {
      result += char.charAt(Math.floor(Math.random() * charLength));
    }
    this.result = result;

    this.id.forEach((data) => {
      this.firestore.collection('users').doc(data).update({
        key_code: result
      })
    })
    const modalRef = this.modalService.open(AdminPopUpRandomComponent);
    const sendValue = modalRef.componentInstance;
    sendValue.result = this.result;
  }

  addParcel(content: any): void {
    this.modalService.open(content);
  }
  changeState_search(state: number, select: string) {
    this.stateSearch = state;
    this.selectSearch = select;
  }
  resetForm() {
    this.searchName('');
    this.searchTrack('');
    this.searchDate('');
  }
  filterItems(arr: any[], query: string) {
    return arr.filter(function (el) {
      return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
  }
  searchName(name: string) {
    this.search_Name.emit(name);
    this.name = name;
    this.search();
  }
  searchReceiver(receiver: string) {
    this.search_Name.emit(receiver);
    this.receiverName = receiver;
    this.search();
  }
  searchTrack(track: string) {
    this.search_Name.emit(track);
    this.track = track;
    this.search();
  }
  searchDate(date: string) {
    this.search_Name.emit(date);
    let dateTime = new Date(date),
      month = '' + (dateTime.getMonth() + 1),
      day = '' + dateTime.getDate(),
      year = '' + dateTime.getFullYear();
    this.date = date;
    this.search();
    return [year, month, day].join('-');
  }
  search() {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    const email = token.forEach((token) => {
      this.users$.subscribe((user) => {
        user.forEach((data_users) => {
          if (data_users.email === token.email) {
            this.deCodeName = data_users.firstname + data_users.lastname;
          }
        });
      });
    });
    this.listWaitParcels$.subscribe((data) => {
      return (this.list_Parcel0 = []), (this.list_Parcel1 = []);
    });
    this.listWaitParcels$.subscribe((dataFromFireStore) => {
      dataFromFireStore.forEach((item) => {
        const data_name = this.deCodeName;
        const item_owner = item.owner.firstname + item.owner.lastname;
        const item_receiver = item.receiver.firstname + item.receiver.lastname;
        this.receiver.push(item_receiver);
        this.owner.push(item_owner);
        this.trackNumber.push(item.trackNumber);
        if (item.checkOut === null) {
          const setDate = (itemD: any) => {
            let dateTime = new Date(itemD),
              month = '' + (dateTime.getMonth() + 1),
              day = '' + dateTime.getDate(),
              year = '' + dateTime.getFullYear();
            if (month.length < 2) {
              month = '0' + month;
            }
            if (day.length < 2) {
              day = '0' + day;
            }
            return [year, month, day].join('-');
          };
          const setD = setDate(item.checkIn);
          this.timeStamp.push(setD);
          const filter_name = this.filterItems(this.owner, this.name);
          filter_name.forEach((filter) => {
            let name = '';
            for (let i of filter) {
              name += i;
            }
            this.name_Filter = name;
          });
          const filter_receiver = this.filterItems(
            this.receiver,
            this.receiverName
          );
          filter_receiver.forEach((filter) => {
            let receiver = '';
            for (let i of filter) {
              receiver += i;
            }
            this.receiver_Filter = receiver;
          });
          const filter_track = this.filterItems(this.trackNumber, this.track);
          filter_track.forEach((filter) => {
            let track = '';
            for (let i of filter) {
              track += i;
            }
            this.track_Filter = track;
          });
          const filter_date = this.filterItems(this.timeStamp, this.date);
          filter_date.forEach((filter) => {
            let date = '';
            for (let i of filter) {
              date += i;
            }
            this.date_Filter = date;
          });
          if (
            item_owner === this.name_Filter &&
            item_receiver === this.receiver_Filter &&
            item.trackNumber === this.track_Filter &&
            setD === this.date_Filter
          ) {
            this.list_Parcel0.push(item);
            this.statusParcels = true;
          }
        } else {
          this.list_Parcel1.push(item);
        }
      });
    });
  }
  activeStatus(p: number) {
    this.statusPage = p;
  }
}
@Component({
  selector: 'random-key',
  template: `
    <div class="p-3">
      <div class="modal-title">
        <button
          type="button"
          class="close"
          aria-label="Close"
          (click)="activeModal.dismiss('Cross click')"
          (click)="close_randomCode()"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header border-0"></div>
      <div class="modal-body border-0">
          <div class="row mt-3">
            <div class="">
              <h4 class="text-center">รหัสผ่านสำหรับการลงทะเบียน</h4>
              <h4 class="text-center">และแก้ไขข้อมูลผู้ใช้งาน</h4>
            </div>
            <div class="col d-flex justify-content-center mt-3">
              <div class="input-group mb-3">
                <div class="form-control text-center resultCode">
                  <h4>{{ result }}</h4>
                </div>
                <div class="input-group-append">
                  <button
                    (click)="random()"
                    class="btnRefreshCode btn btn-outline-secondary"
                    type="button"
                    id="button-addon2"
                  >
                    <mat-icon>refresh</mat-icon>
                  </button>
                </div>
              </div>
            </div>
            <p class="text-danger">*** หากปิดหน้าต่างนี้ รหัสต่อไปนี้จะไม่สามารถใช้งานได้ ***</p>
          </div>
        </div>
    </div>
  `,
  styles: [
    `
      .resultCode {
        border-color: #30009c;
      }
      .btnRefreshCode {
        border-color: #30009c;
      }
      mat-icon {
        color: #30009c;
      }
    `,
  ],
})
export class AdminPopUpRandomComponent {
  @Input() result: string = '';
  data_usrs: Observable<any[]>;
  id: any[] = [];
  constructor(public activeModal: NgbActiveModal, private aft: AngularFirestore, configModal: NgbModalConfig) {
    this.data_usrs = aft.collection('users').valueChanges({ idField: 'ID' });
    this.data_usrs.subscribe(data => {
      data.forEach(users => {
        if (users.role === 'admin') {
          this.id.push(users.ID);
        }
      })
    })
    configModal.backdrop = 'static';
    configModal.keyboard = false;
  }
  ngOnInit(): void {
  }
  random() {
    let result = '';
    const char =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charLength = char.length;
    for (let i = 0; i < 6; i++) {
      result += char.charAt(Math.floor(Math.random() * charLength));
    }
    this.result = result;

    this.id.forEach((data) => {
      this.aft.collection('users').doc(data).update({
        key_code: result
      })
    })
  }
  close_randomCode() {
    this.id.forEach((data) => {
      this.aft.collection('users').doc(data).update({
        key_code: null
      })
    })
  }
}
