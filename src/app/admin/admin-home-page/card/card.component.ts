import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbCarouselConfig, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { StatusParcelsModel } from 'src/app/@common/enums/statusParcels.enum';
import { ParcelsModel } from 'src/app/@common/model/parcels';
import { AgentModel, UsersModel } from 'src/app/@common/model/users';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() listParcels: ParcelsModel = {};
  @Input() statusParcels: StatusParcelsModel = 0;
  @Input() owner: string = '';
  data_usrs: Observable<any[]>;
  ownerName: string = '';
  editBtn: boolean = false;
  nDate = new Date();
  date: string = '';
  time: string = '';
  listImage!: { Img0: Observable<any>; Img1: Observable<any>; Img2: Observable<any>; Img3: Observable<any>; };
  storage!: Observable<string | null>;
  owner_email: string = '';
  owner_password: string = '';
  ag0_fname: string = '';
  ag0_lname: string = '';
  ag0_email: string = '';
  ag0_status: boolean = false;
  ag1_fname: string = '';
  ag1_lname: string = '';
  ag1_email: string = '';
  ag1_status: boolean = false;
  ag2_fname: string = '';
  ag2_lname: string = '';
  ag2_email: string = '';
  ag2_status: boolean = false;
  emailToken: string = '';
  firstname: string = '';
  lastname: string = '';
  constructor(public modalService: NgbModal, public angularstorage: AngularFireStorage, config: NgbCarouselConfig, private aft: AngularFirestore) {
    this.data_usrs = aft.collection('users').valueChanges();
  }
  ngOnInit(): void {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(data => {
      this.emailToken = data.email
    });
    this.data_usrs.subscribe((data) => {
      data.forEach(users => {
        if (users.email === this.emailToken) {
          this.firstname = users.firstname;
          this.lastname = users.lastname;
        }
      })
    })
    this.ownerName = `${this.listParcels.owner?.firstname}` + `${this.listParcels.owner?.lastname}`;
    const Img0 = this.angularstorage.ref(`${this.listParcels.images?.image0}`).getDownloadURL();
    const Img1 = this.angularstorage.ref(`${this.listParcels.images?.image1}`).getDownloadURL();
    const Img2 = this.angularstorage.ref(`${this.listParcels.images?.image2}`).getDownloadURL();
    const Img3 = this.angularstorage.ref(`${this.listParcels.images?.image3}`).getDownloadURL();
    const listImg = { Img0, Img1, Img2, Img3 };
    this.listImage = listImg;
    const ndate = new Date(`${this.listParcels.checkIn}`);
    const resultDate = ndate.toLocaleDateString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
    const resultTime = ndate.toLocaleTimeString('th-TH', {
      hour: '2-digit',
      minute: 'numeric'
    });
    this.date = resultDate;
    this.time = resultTime;
  }
  showImage(content: any): void {
    this.modalService.open(content);
  }
  editParcel() {
    const modalRef = this.modalService.open(AdminEditParcelComponent);
    const sendValue = modalRef.componentInstance;
    sendValue.data = this.listParcels;
  }
  open_popup_Checkout() {
    const modalRef = this.modalService.open(AdminPopUpCheckOutComponent);
    const sendValue = modalRef.componentInstance;
    this.data_usrs.subscribe((data) => {
      data.forEach((users) => {
        if (users.firstname === this.listParcels.owner?.firstname && users.lastname === this.listParcels.owner?.lastname) {
          this.owner_email = `${users.email}`;
          this.owner_password = `${users.password}`;
          // if (users.agent?.agent0?.verify === true) {
          this.ag0_fname = `${users.agent?.agent0?.firstname}`;
          this.ag0_lname = `${users.agent?.agent0?.lastname}`;
          this.ag0_status = users.agent?.agent0?.status_receiver;
          // }
          // if (users.agent?.agent1?.verify === true) {
          this.ag1_fname = `${users.agent?.agent1?.firstname}`;
          this.ag1_lname = `${users.agent?.agent1?.lastname}`;
          this.ag1_status = users.agent?.agent1?.status_receiver;

          // }
          // if (users.agent?.agent2?.verify === true) {
          this.ag2_fname = `${users.agent?.agent2?.firstname}`;
          this.ag2_lname = `${users.agent?.agent2?.lastname}`;
          this.ag2_status = users.agent?.agent2?.status_receiver;
          // }
        }
      });
      const owner_pass = atob(this.owner_password);
      sendValue.owner_email = this.owner_email;
      sendValue.owner_password = owner_pass;
      sendValue.ag0_fname = this.ag0_fname;
      sendValue.ag0_lname = this.ag0_lname;
      sendValue.ag0_status = this.ag0_status;
      sendValue.ag1_fname = this.ag1_fname;
      sendValue.ag1_lname = this.ag1_lname;
      sendValue.ag1_status = this.ag1_status;
      sendValue.ag2_fname = this.ag2_fname;
      sendValue.ag2_lname = this.ag2_lname;
      sendValue.ag2_status = this.ag2_status;
    });
    sendValue.firstname = this.firstname;
    sendValue.lastname = this.lastname;
    sendValue.data = this.listParcels;
  }
}
@Component({
  selector: 'admin-edit-parcel',
  template: `<div class="p-3">
    <div class="modal-title">
      <button mat-stroked-button color="warn" class="border-danger rounded-pill" (click)="removeParcel()">
        ลบรายการพัสดุ
        <mat-icon>delete</mat-icon>
      </button>
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="activeModal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-header border-0">
      <div class="row">
        <div class="d-flex justify-content-center">
          <img src="../../../assets/images/box.svg" class="w-50" alt="" />
        </div>
        <div class="d-flex justify-content-center mt-3">
          <strong>แก้ไขรายการพัสดุ</strong>
        </div>
      </div>
    </div>
    <div class="modal-body">
      <div class="row">
        <p>หมายเลขติดตามพัสดุ</p>
      </div>
      <div class="row">
        <input
          type="text"
          class="text-center"
          [(ngModel)]="track"
          placeholder="กรอกหมายเลขติดตามพัสดุ"
        />
      </div>
      <div class="row mt-3">
        <button
          class="btnedit"
          mat-flat-button
          (click)="editTracking()"
          (click)="activeModal.dismiss('Cross click')"
        >
          บันทึก
        </button>
      </div>
    </div>
  </div>`,
  styles: [
    `
    .btnedit{
        background-color: #30009C;
        color: white;
    }
}`,
  ],
})
export class AdminEditParcelComponent {
  @Input() protected data: ParcelsModel = {};
  track: string = '';
  CRUD: any;
  id: any[] = [];
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.CRUD = aft.collection('parcels');
  }
  ngOnInit(): void {
    this.track = `${this.data.trackNumber}`;
    this.id.push(this.data);
    this.id.forEach((data) => {
      return (this.id = data.ID);
    });
  }

  removeParcel() {
    if (confirm('ยืนยันลบรายการพัสดุหมายเลข : ' + this.track)) {
      try {
        this.CRUD.doc(this.id).delete();
      } catch {
        alert('ลบรายการพัสดุไม่สำเร็จ')
      } finally {
        this.snackBar.openFromComponent(PopUpAdminRemoveParcelComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 15000,
          panelClass: ['custom-css-class'],
        });
        this.activeModal.dismiss();
      }
    }
  }
  editTracking() {
    try {
      this.CRUD.doc(this.id).update({
        owner: {
          firstname: this.data.owner?.firstname,
          lastname: this.data.owner?.lastname,
        },
        receiver: {
          firstname: this.data.receiver?.firstname,
          lastname: this.data.receiver?.lastname,
        },
        trackNumber: this.track,
        department: this.data.department,
        checkIn: this.data.checkIn,
        checkOut: this.data.checkOut,
        images: {
          image0: this.data.images?.image0,
          image1: this.data.images?.image1,
          image2: this.data.images?.image2,
          image3: this.data.images?.image3,
        },
      });
    } catch (err) {
      alert('แก้ไขหมายเลขติดตามพัสดุไม่สำเร็จ');
    } finally {
      this.snackBar.openFromComponent(PopUpAdminAddTrackingSuccessComponent, {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 15000,
        panelClass: ['custom-css-class'],
      });
    }
  }
}
@Component({
  selector: 'admin-checkOut-parcel',
  template: `<div class="p-3">
    <div class="modal-title">
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="activeModal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="d-flex justify-content-center mt-3">
          <strong>ยืนยันการรับพัสดุ</strong>
        </div>
    </div>
    <div class="modal-header border-0">
    <div class="row">
        <div class="col d-flex justify-content-center">
          <img src="../../../assets/images/box.svg" class="w-25" alt="" />
        </div>
        <div class="col">
          <p>หมายเลขติดตามพัสดุ</p>
          <p>{{data.trackNumber}}</p> 
        </div>
      </div>
    </div>
    <div class="modal-body">
      <div class="row">
      <p class="text-danger">*กรอกที่อยู่อีเมลและรหัสผ่านโดยผู้รับพัสดุ เพื่อยืนยันสิทธิ</p>
      <div>
        <input
            type="email"
            class="form-control mt-2"
            placeholder="ที่อยู่อีเมล"
            [(ngModel)]="email"
            required
            [class]="required_email"
          />
        <div class="invalid-feedback" *ngIf="check_email ==='not pass'">
          กรุณากรอกที่อยู่อีเมล
        </div>
      </div>
      <div>
      <input
          type="password"
          class="form-control mt-2"
          placeholder="รหัสผ่าน"
          [(ngModel)]="password"
          required
          [class]="required_password"
        />
        <div class="invalid-feedback" *ngIf="check_password ==='not pass'">
          กรูณากรอกรหัสผ่าน
        </div>
      </div>
      </div>
      <div class="mt-3 d-flex justify-content-center">
        <button mat-flat-button class="btncancle col-4 mr-2" (click)="activeModal.dismiss('Cross click')">ยกเลิก</button>
        <button
          class="btnedit col-4 ml-2"
          mat-flat-button
          (click)="checkOut()"
        >
          ยืนยัน
        </button>
      </div>
    </div>
  </div>`,
  styles: [
    `
    .btnedit{
        background-color: #30009C;
        color: white;
    }
    .btncancle{
      background-color:#F0F2F5;
    }
}`,
  ],
})
export class AdminPopUpCheckOutComponent {
  @Input() data: ParcelsModel = {};
  @Input() firstname: string = '';
  @Input() lastname: string = '';
  required_email: string = '';
  check_email: string = '';
  required_password: string = '';
  check_password: string = '';
  id: any[] = []
  data_usrs: Observable<any[]>;
  email: string = '';
  password: string = '';
  @Input() public owner_email: string = '';
  @Input() private owner_password: string = '';
  @Input() public ag0_fname: string = '';
  @Input() public ag0_lname: string = '';
  @Input() public ag0_status: boolean = false;
  ag0_email: string = '';
  ag0_password: string = '';
  @Input() public ag1_fname: string = '';
  @Input() public ag1_lname: string = '';
  @Input() public ag1_status: boolean = false;
  ag1_email: string = '';
  ag1_password: string = '';
  @Input() public ag2_fname: string = '';
  @Input() public ag2_lname: string = '';
  @Input() public ag2_status: boolean = false;
  ag2_email: string = '';
  ag2_password: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.data_usrs = aft.collection('users').valueChanges();
  }
  ngOnInit(): void {
    this.id.push(this.data);
    this.id.forEach((data) => {
      return (this.id = data.ID);
    });
    this.data_usrs.subscribe(data => {
      data.forEach((users: UsersModel) => {
        if (users.firstname === this.ag0_fname && users.lastname === this.ag0_lname) {
          this.ag0_email = `${users.email}`;
          this.ag0_password = atob(`${users.password}`);
        }
        if (users.firstname === this.ag1_fname && users.lastname === this.ag1_lname) {
          this.ag1_email = `${users.email}`;
          this.ag1_password = atob(`${users.password}`);
        }
        if (users.firstname === this.ag2_fname && users.lastname === this.ag2_lname) {
          this.ag2_email = `${users.email}`;
          this.ag2_password = atob(`${users.password}`);
        }
      })
    })
  }
  checkOut() {
    const email = this.email;
    const password = this.password;
    if (this.data.checkIn === null) {
      alert('พัสดุยังไม่มีการรับโดยเจ้าหน้าที่');
      this.activeModal.dismiss();
    } else {
      if (email === '') {
        this.required_email = 'is-invalid';
        this.check_email = 'not pass';
      } else {
        this.required_email = '';
        this.check_email = '';
      }
      if (password === '') {
        this.required_password = 'is-invalid';
        this.check_password = 'not pass';
      } else {
        this.required_password = '';
        this.check_password = '';
      }
      if (email !== '' && password !== '') {
        if (email === this.owner_email && password === this.owner_password) {
          this.aft.collection('parcels').doc(`${this.id}`).update({
            checkOut: Date(),
            receiver: {
              firstname: this.data.owner?.firstname,
              lastname: this.data.owner?.lastname
            }
          }).then(() => {
            this.data_usrs.subscribe((data) => data.forEach(user => {
              if (user.role === 'admin') {
                this.send_message(`${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${user.firstname}`, `${user.lastname}`, `${user.email}`);
              }
            }));
            this.send_message(`${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.owner_email}`);
            if (this.ag0_fname !== '' && this.ag0_lname !== '' && this.ag0_status === true) {
              this.send_message(`${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.ag0_fname}`, `${this.ag0_lname}`, this.ag0_email);
            }
            if (this.ag1_fname !== '' && this.ag1_lname !== '' && this.ag1_status === true) {
              this.send_message(`${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.ag1_fname}`, `${this.ag1_lname}`, this.ag1_email);
            }
            if (this.ag2_fname !== '' && this.ag2_lname !== '' && this.ag2_status === true) {
              this.send_message(`${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.ag2_fname}`, `${this.ag2_lname}`, this.ag2_email);
            }
          }).catch((err) => {
            alert('ไม่สามารถรับพัสดุได้');
          });
        } else if (email === this.ag0_email && password === this.ag0_password) {
          this.aft.collection('parcels').doc(`${this.id}`).update({
            checkOut: Date(),
            receiver: {
              firstname: this.ag0_fname,
              lastname: this.ag0_lname
            }
          }).then(() => {
            this.data_usrs.subscribe((data) => data.forEach(user => {
              if (user.role === 'admin') {
                this.send_message(this.ag0_fname, this.ag0_lname, `${user.firstname}`, `${user.lastname}`, `${user.email}`);
              }
            }));
            this.send_message(this.ag0_fname, this.ag0_lname, `${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.owner_email}`);
            if (this.ag0_fname !== '' && this.ag0_lname !== '' && this.ag0_status === true) {
              this.send_message(this.ag0_fname, this.ag0_lname, `${this.ag0_fname}`, `${this.ag0_lname}`, this.ag0_email);
            }
            if (this.ag1_fname !== '' && this.ag1_lname !== '' && this.ag1_status === true) {
              this.send_message(this.ag0_fname, this.ag0_lname, `${this.ag1_fname}`, `${this.ag1_lname}`, this.ag1_email);
            }
            if (this.ag2_fname !== '' && this.ag2_lname !== '' && this.ag2_status === true) {
              this.send_message(this.ag0_fname, this.ag0_lname, `${this.ag2_fname}`, `${this.ag2_lname}`, this.ag2_email);
            }
          }).catch((err) => {
            alert('ไม่สามารถรับพัสดุได้');
          });
        } else if (email === this.ag1_email && password === this.ag1_password) {
          this.aft.collection('parcels').doc(`${this.id}`).update({
            checkOut: Date(),
            receiver: {
              firstname: this.ag1_fname,
              lastname: this.ag1_lname
            }
          }).then(() => {
            this.data_usrs.subscribe((data) => data.forEach(user => {
              if (user.role === 'admin') {
                this.send_message(this.ag1_fname, this.ag1_lname, `${user.firstname}`, `${user.lastname}`, `${user.email}`);
              }
            }));
            this.send_message(this.ag1_fname, this.ag1_lname, `${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.owner_email}`);
            if (this.ag0_fname !== '' && this.ag0_lname !== '' && this.ag0_status === true) {
              this.send_message(this.ag1_fname, this.ag1_lname, `${this.ag0_fname}`, `${this.ag0_lname}`, this.ag0_email);
            }
            if (this.ag1_fname !== '' && this.ag1_lname !== '' && this.ag1_status === true) {
              this.send_message(this.ag1_fname, this.ag1_lname, `${this.ag1_fname}`, `${this.ag1_lname}`, this.ag1_email);
            }
            if (this.ag2_fname !== '' && this.ag2_lname !== '' && this.ag2_status === true) {
              this.send_message(this.ag1_fname, this.ag1_lname, `${this.ag2_fname}`, `${this.ag2_lname}`, this.ag2_email);
            }
          }).catch((err) => {
            alert('ไม่สามารถรับพัสดุได้');
          });
        } else if (email === this.ag2_email && password === this.ag2_password) {
          this.aft.collection('parcels').doc(`${this.id}`).update({
            checkOut: Date(),
            receiver: {
              firstname: this.ag2_fname,
              lastname: this.ag2_lname
            }
          }).then(() => {
            this.data_usrs.subscribe((data) => data.forEach(user => {
              if (user.role === 'admin') {
                this.send_message(this.ag2_fname, this.ag2_lname, `${user.firstname}`, `${user.lastname}`, `${user.email}`);
              }
            }));
            this.send_message(this.ag2_fname, this.ag2_lname, `${this.data.owner?.firstname}`, `${this.data.owner?.lastname}`, `${this.owner_email}`);
            if (this.ag0_fname !== '' && this.ag0_lname !== '' && this.ag0_status === true) {
              this.send_message(this.ag2_fname, this.ag2_lname, `${this.ag0_fname}`, `${this.ag0_lname}`, this.ag0_email);
            }
            if (this.ag1_fname !== '' && this.ag1_lname !== '' && this.ag1_status === true) {
              this.send_message(this.ag2_fname, this.ag2_lname, `${this.ag1_fname}`, `${this.ag1_lname}`, this.ag1_email);
            }
            if (this.ag2_fname !== '' && this.ag2_lname !== '' && this.ag2_status === true) {
              this.send_message(this.ag2_fname, this.ag2_lname, `${this.ag2_fname}`, `${this.ag2_lname}`, this.ag2_email);
            }
          }).catch((err) => {
            alert('ไม่สามารถรับพัสดุได้');
          });
        } else {
          alert('ไม่มีสิทธิในการรับพัสดุชิ้นนี้');
        }
      }
    }
  }
  send_message(receiver_parcel_fname: string, receiver_parcel_lname: string, receiver_fname: string, receiver_lname: string, email: string) {
    this.aft.collection('messages').add({
      data: {
        receiver_parcel: {
          firstname: receiver_parcel_fname,
          lastname: receiver_parcel_lname
        },
        owner: {
          firstname: this.data.owner?.firstname,
          lastname: this.data.owner?.lastname
        },
        trackNumber: this.data.trackNumber
      },
      dateTime: Date(),
      read: false,
      receiver: {
        firstname: receiver_fname,
        lastname: receiver_lname,
        email: email
      },
      sender: {
        firstname: this.firstname,
        lastname: this.lastname
      },
      type: 0
    }).then(() => {
      this.activeModal.dismiss();
      setTimeout(()=>{
        this.snackBar.openFromComponent(PopUpAdminCheckOutParcelComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
      },300)
      setTimeout(()=>{
        location.reload();
      },2000)
    })
  }
}
@Component({
  selector: 'admin-add-tracking',
  template: `<span>
    <img style="width:3vw" src="../../../assets/images/box.svg" alt="" />
    <strong class="ms-5">แก้ไขรายการพัสดุเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpAdminAddTrackingSuccessComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}

@Component({
  selector: 'remove-parcel',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-danger">delete</mat-icon>
    <strong class="ms-5">ลบรายการพัสดุเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpAdminRemoveParcelComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
@Component({
  selector: 'check-out-parcel',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-success">check_circle</mat-icon>
    <strong class="ms-5">รับพัสดุเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpAdminCheckOutParcelComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}

