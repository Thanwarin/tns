import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCardHistoryComponent } from './admin-card-history.component';

describe('AdminCardHistoryComponent', () => {
  let component: AdminCardHistoryComponent;
  let fixture: ComponentFixture<AdminCardHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminCardHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCardHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
