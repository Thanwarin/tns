import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminListHistoryComponent } from './admin-list-history.component';

describe('AdminListHistoryComponent', () => {
  let component: AdminListHistoryComponent;
  let fixture: ComponentFixture<AdminListHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminListHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminListHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
