import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { formatsAvailable } from 'src/app/barcode-formats';
import { MatSnackBar } from '@angular/material/snack-bar';
import jwtDecode from 'jwt-decode';

import { DomSanitizer } from '@angular/platform-browser';
import { ParcelsModel } from 'src/app/@common/model/parcels';
import { AngularFireStorage } from '@angular/fire/storage';
@Component({
  selector: 'app-admin-scan',
  templateUrl: './admin-scan.component.html',
  styleUrls: ['./admin-scan.component.scss'],
})
export class AdminScanComponent implements OnInit {
  @ViewChild('content') private content: any;
  formats = formatsAvailable;
  openCamera = true;
  tracking: any[] = [];
  device: any;
  devices: MediaDeviceInfo[] = [];
  result: Array<string> = [];
  tryHarder = false;
  dbParcels$: Observable<any[]>;
  availableDevices!: MediaDeviceInfo[];
  currentDevice!: MediaDeviceInfo | null;
  hasDevices!: boolean;
  hasPermission!: boolean;
  list_trackNumber: Array<string> = [];
  list_users$: Observable<any[]>;
  email: string = '';
  nameUser: any[] = [];
  list_name: any[] = [];
  name: string = '';
  list_Tracking = '';
  checkTrack: Array<number> = [];
  i: number = 0;
  track: string = '';
  tracking_Data: any[] = []
  check: EventEmitter<string> = new EventEmitter();
  check_Type0: any[] = [];
  check_Type1: any[] = [];
  file: any;
  namefile: string = '';
  CRUD: any;
  id: string = '';
  data: any = {};
  parcels: ParcelsModel[] = [];
  users: any[] = [];
  firstname_owner: string = '';
  lastname_owner: string = '';
  firstname: string = '';
  lastname: string = '';
  check_camera: boolean = false;
  email_owner: string = '';
  fname_ag0: string = '';
  lname_ag0: string = '';
  email_ag0: string = '';
  status_ag0: boolean = false;
  fname_ag1: string = '';
  lname_ag1: string = '';
  email_ag1: string = '';
  status_ag1: boolean = false;
  fname_ag2: string = '';
  lname_ag2: string = '';
  email_ag2: string = '';
  status_ag2: boolean = false;
  constructor(configModal: NgbModalConfig, private modalService: NgbModal, private snackBar: MatSnackBar, private aft: AngularFirestore, public sanitizer: DomSanitizer, private afstorage: AngularFireStorage) {
    this.dbParcels$ = aft.collection('parcels').valueChanges({ idField: 'ID' });
    this.dbParcels$.subscribe((data) => {
      data.forEach((parcels) => {
        this.parcels.push(parcels)
      })
    })
    this.CRUD = aft.collection('parcels');
    this.list_users$ = aft.collection('users').valueChanges();
    this.list_users$.subscribe((data) => {
      data.forEach((users) => {
        this.users.push(users)
      })
    })
    configModal.backdrop = 'static';
    configModal.keyboard = false;
  }

  ngOnInit(): void {
    try {
      this.currentDevice = null;
    } catch (err) {

    }
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(tk_email => {
      this.email = tk_email.email;
    });
    this.list_users$.subscribe(data => {
      data.forEach(user => {
        if (this.email === user.email) {
          this.firstname = user.firstname;
          this.lastname = user.lastname;
        }
        this.nameUser.push(user.firstname + " " + user.lastname);
        const remove = (arr: any) => [...new Set(arr)];
        this.list_name = remove(this.nameUser)
      });
    });
    this.dbParcels$.subscribe(data => {
      data.forEach(parcels => {
        if (parcels.checkOut === null) {
          this.tracking_Data.push(parcels)
        }
      });
    });
  }

  ngOnChanges(changes: SimpleChange): void {
    this.modalService.open(this.content);
  }
  addParcel(content: any): void {
    this.modalService.open(content);
  }

  camerasFoundHandler(d: MediaDeviceInfo[]) {
    if (d.length > 0) {
      this.check_camera = true;
    }
    this.availableDevices = d;
    this.devices = d;
    this.i = this.devices.length - 1;
  }
  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }
  changeCamera(change: any) {
    const count = this.devices.length;
    if (this.i < count && this.i !== 0) {
      this.i--
    } else if (this.i === 0) {
      this.i = count - 1;
    }
    this.currentDevice = this.devices[this.i]
  }
  scanSuccessHandler(result: any) {
    try {
      this.list_Tracking = result;
      this.openPopUp(result);

    } catch (err) {
      console.log(err);
    }
  }
  cancel() {
    this.tracking = [];
  }
  openPopUp(message: string) {
    if (confirm('หมายเลขติดตามพัสดุ : ' + message)) {
      const check_list = {
        tracking: this.list_Tracking,
        check: 1,
        img0: '',
        path0: null,
        img1: '',
        path1: null,
        img2: '',
        path2: null,
        img3: '',
        path3: null,
      }

      const setArr = this.tracking.find(data => data.tracking === check_list.tracking)
      if (setArr === undefined) {
        this.tracking.push(check_list);
      }
      this.modalService.open(this.content);
      this.check_Parcels();
    }
  }
  addTrack(e: any) {
    if (this.track !== '') {
      const check_list = {
        tracking: this.track,
        check: 1,
        img0: '',
        path0: null,
        img1: '',
        path1: null,
        img2: '',
        path2: null,
        img3: '',
        path3: null,
      }
      const setArr = this.tracking.find(data => data.tracking === check_list.tracking)
      if (setArr === undefined) {
        this.tracking.push(check_list);
      }
    }
    this.track = '';
    this.check_Parcels();
  }
  deleteTrack(track: any) {
    function arrayRemove(arr: any, value: any) {
      return arr.filter(function (ele: any) {
        return ele != value;
      });
    }
    const list_track = this.tracking.find(data => data.tracking === track)
    const result = arrayRemove(this.tracking, list_track);
    this.tracking = result;
  }
  addTracking(e: any) {
    this.modalService.dismissAll();
    this.track = '';
  }
  search(e: string) {
    this.check.emit(e)
    if (this.check) {
      this.check_Parcels();
    }
    this.users.forEach((users) => {
      if (users.firstname + ' ' + users.lastname === this.name) {
        this.firstname_owner = `${users.firstname}`;
        this.lastname_owner = `${users.lastname}`;
        this.email_owner = `${users.email}`;
        this.fname_ag0 = `${users.agent?.agent0.firstname}`;
        this.lname_ag0 = `${users.agent?.agent0.lastname}`;
        this.email_ag0 = `${users.agent?.agent0.email}`;
        this.status_ag0 = users.agent?.agent0.status_receiver;

        this.fname_ag1 = `${users.agent?.agent1.firstname}`;
        this.lname_ag1 = `${users.agent?.agent1.lastname}`;
        this.email_ag1 = `${users.agent?.agent1.email}`;
        this.status_ag1 = users.agent?.agent1.status_receiver;

        this.fname_ag2 = `${users.agent?.agent2.firstname}`;
        this.lname_ag2 = `${users.agent?.agent2.lastname}`;
        this.email_ag2 = `${users.agent?.agent2.email}`;
        this.status_ag2 = users.agent?.agent2.status_receiver;
      }
    })
  }
  check_Parcels() {
    const remove = (arr: any) => [...new Set(arr)];
    if (this.name !== "") {
      this.tracking_Data.forEach((data) => {
        if (data.owner.firstname + " " + data.owner.lastname === this.name) {
          this.check_Type0.push(data.trackNumber);
          remove(this.check_Type0);
        } else {

          this.check_Type1.push(data.trackNumber)
          remove(this.check_Type1);
        }
      });
      this.tracking.forEach((d_track) => {
        const set_type0 = this.check_Type0.find(data0 => data0 === d_track.tracking);
        const set_type1 = this.check_Type1.find(data1 => data1 === d_track.tracking);
        if (set_type0 !== undefined) {
          d_track.check = 0;
        } else if (set_type1 === undefined) {
          d_track.check = 1;
        } else if (set_type1 !== undefined) {
          d_track.check = 2;
        }
      })
    } else if (this.name === "") {
      this.tracking.forEach(data => {
        data.check = 1;
      })
    }
  }
  open_Image_Component(track: object, path0: any, path1: any, path2: any, path3: any) {
    const modalRef = this.modalService.open(AddImageComponent);
    const sendValue = modalRef.componentInstance;
    sendValue.track = track;
    sendValue.path_img0 = path0;
    sendValue.path_img1 = path1;
    sendValue.path_img2 = path2;
    sendValue.path_img3 = path3;
    modalRef.result.then((result) => {
      if (result) {
        this.tracking.forEach(data => {
          if (data.tracking === result.tracking) {
            data.img0 = result.img0;
            data.path0 = result.path0;
            data.img1 = result.img1;
            data.path1 = result.path1;
            data.img2 = result.img2;
            data.path2 = result.path2;
            data.img3 = result.img3;
            data.path3 = result.path3;
          }
        })
      }
    })

  }
  CU_firebase() {
    let n = Date.now();
    let date = Date()
    let image0 = '';
    let image1 = '';
    let image2 = '';
    let image3 = '';
    this.parcels.forEach((parcels: any) => {
      this.tracking.forEach((list) => {
        if (list.check === 0) {
          if (list.tracking === parcels.trackNumber && parcels.checkOut === null) { //ตรวจสอบพัสดุที่มีอยู่แล้ว
            if (list.img0 !== '') {
              image0 = 'Parcels/' + n + list.img0;
            } else {
              image0 = '';
            };
            if (list.img1 !== '') {
              image1 = 'Parcels/' + n + list.img1;
            } else {
              image1 = '';
            };
            if (list.img2 !== '') {
              image2 = 'Parcels/' + n + list.img2;
            } else {
              image2 = '';
            };
            if (list.img3 !== '') {
              image3 = 'Parcels/' + n + list.img3;
            } else {
              image3 = '';
            };
            this.data = {
              owner: {
                firstname: parcels.owner.firstname,
                lastname: parcels.owner.lastname,
              },
              receiver: {
                firstname: parcels.receiver.firstname,
                lastname: parcels.receiver.lastname
              },
              trackNumber: list.tracking,
              department: parcels.department,
              checkIn: `${date}`,
              checkOut: null,
              images: {
                image0: image0,
                image1: image1,
                image2: image2,
                image3: image3
              }
            };
            this.id = parcels.ID;
            this.update_data();
            this.sendNotifyOwner(`${list.tracking}`);
            this.sendNotifyAgent0(`${list.tracking}`);
            this.sendNotifyAgent1(`${list.tracking}`);
            this.sendNotifyAgent2(`${list.tracking}`);

            this.tracking.forEach(data => {
              if (data.path0 !== null) {
                const img0 = this.afstorage.upload(`Parcels/${n + data.path0.target.files[0].name}`, data.path0.target.files[0]);
                img0.snapshotChanges()
              }
              if (data.path1 !== null) {
                const img1 = this.afstorage.upload(`Parcels/${n + data.path1.target.files[0].name}`, data.path1.target.files[0]);
                img1.snapshotChanges()
              }
              if (data.path2 !== null) {
                const img2 = this.afstorage.upload(`Parcels/${n + data.path2.target.files[0].name}`, data.path2.target.files[0]);
                img2.snapshotChanges()
              }
              if (data.path3 !== null) {
                const img3 = this.afstorage.upload(`Parcels/${n + data.path3.target.files[0].name}`, data.path3.target.files[0]);
                img3.snapshotChanges()
              }
            })
          };
        } else if (list.check === 1) {
          // if (list.check === 1) {
          if (list.tracking !== parcels.trackNumber) {
            if (list.img0 !== '') {
              image0 = 'Parcels/' + n + list.img0;
            } else {
              image0 = '';
            };
            if (list.img1 !== '') {
              image1 = 'Parcels/' + n + list.img1;
            } else {
              image1 = '';
            };
            if (list.img2 !== '') {
              image2 = 'Parcels/' + n + list.img2;
            } else {
              image2 = '';
            };
            if (list.img3 !== '') {
              image3 = 'Parcels/' + n + list.img3;
            } else {
              image3 = '';
            };
            this.data = {
              owner: {
                firstname: this.firstname_owner,
                lastname: this.lastname_owner,
              },
              receiver: {
                firstname: '',
                lastname: ''
              },
              trackNumber: list.tracking,
              department: '',
              checkIn: `${date}`,
              checkOut: null,
              images: {
                image0: image0,
                image1: image1,
                image2: image2,
                image3: image3
              }
            };
            this.sendNotifyOwner(`${list.tracking}`);
            this.sendNotifyAgent0(`${list.tracking}`);
            this.sendNotifyAgent1(`${list.tracking}`);
            this.sendNotifyAgent2(`${list.tracking}`);

            this.tracking.forEach(data => {
              if (data.path0 !== null) {
                const img0 = this.afstorage.upload(`Parcels/${n + data.path0.target.files[0].name}`, data.path0.target.files[0]);
                img0.snapshotChanges()
              }
              if (data.path1 !== null) {
                const img1 = this.afstorage.upload(`Parcels/${n + data.path1.target.files[0].name}`, data.path1.target.files[0]);
                img1.snapshotChanges()
              }
              if (data.path2 !== null) {
                const img2 = this.afstorage.upload(`Parcels/${n + data.path2.target.files[0].name}`, data.path2.target.files[0]);
                img2.snapshotChanges()
              }
              if (data.path3 !== null) {
                const img3 = this.afstorage.upload(`Parcels/${n + data.path3.target.files[0].name}`, data.path3.target.files[0]);
                img3.snapshotChanges()
              }
            })
          }
        }
      })
    })
  }
  sendNotifyOwner(track: string) {
    this.aft.collection('messages').add({
      data: {
        receiver_parcel: {
          firstname: '',
          lastname: ''
        },
        owner: {
          firstname: this.firstname_owner,
          lastname: this.lastname_owner
        },
        trackNumber: track
      },
      dateTime: Date(),
      read: false,
      receiver: {
        firstname: this.firstname_owner,
        lastname: this.lastname_owner,
        email: this.email_owner
      },
      sender: {
        firstname: this.firstname,
        lastname: this.lastname
      },
      type: 2
    });
  }
  sendNotifyAgent0(track: string) {
    if (this.status_ag0 === true && this.fname_ag0 !== '' && this.lname_ag0 !== '') {
      this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 1
        data: {
          receiver_parcel: {
            firstname: '',
            lastname: ''
          },
          owner: {
            firstname: this.firstname_owner,
            lastname: this.firstname_owner
          },
          trackNumber: track
        },
        dateTime: Date(),
        read: false,
        receiver: {
          firstname: this.fname_ag0,
          lastname: this.lname_ag0,
          email: this.email_ag0
        },
        sender: {
          firstname: this.firstname,
          lastname: this.lastname
        },
        type: 2
      });
    }
  }
  sendNotifyAgent1(track: string) {
    if (this.status_ag1 === true && this.fname_ag1 !== '' && this.lname_ag1 !== '') {
      this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 2
        data: {
          receiver_parcel: {
            firstname: '',
            lastname: ''
          },
          owner: {
            firstname: this.firstname_owner,
            lastname: this.firstname_owner
          },
          trackNumber: track
        },
        dateTime: Date(),
        read: false,
        receiver: {
          firstname: this.fname_ag1,
          lastname: this.lname_ag1,
          email: this.email_ag1
        },
        sender: {
          firstname: this.firstname,
          lastname: this.lastname
        },
        type: 2
      });
    }
  }
  sendNotifyAgent2(track: string) {
    if (this.status_ag2 === true && this.fname_ag2 !== '' && this.lname_ag2 !== '') {
      this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 3
        data: {
          receiver_parcel: {
            firstname: '',
            lastname: ''
          },
          owner: {
            firstname: this.firstname_owner,
            lastname: this.firstname_owner
          },
          trackNumber: track
        },
        dateTime: Date(),
        read: false,
        receiver: {
          firstname: this.fname_ag2,
          lastname: this.lname_ag2,
          email: this.email_ag2
        },
        sender: {
          firstname: this.firstname,
          lastname: this.lastname
        },
        type: 2
      });
    }
  }
  update_data() {
    this.CRUD.doc(this.id).update(this.data);
  }
  add_data() {
    this.CRUD.add(this.data)
  }
  uploadImage() {
    let n = Date.now();
    this.tracking.forEach(data => {
      if (data.path0 !== null) {
        const img0 = this.afstorage.upload(`Parcels/${n + data.path0.target.files[0].name}`, data.path0.target.files[0]);
        img0.snapshotChanges()
      }
      if (data.path1 !== null) {
        const img1 = this.afstorage.upload(`Parcels/${n + data.path1.target.files[0].name}`, data.path1.target.files[0]);
        img1.snapshotChanges()
      }
      if (data.path2 !== null) {
        const img2 = this.afstorage.upload(`Parcels/${n + data.path2.target.files[0].name}`, data.path2.target.files[0]);
        img2.snapshotChanges()
      }
      if (data.path3 !== null) {
        const img3 = this.afstorage.upload(`Parcels/${n + data.path3.target.files[0].name}`, data.path3.target.files[0]);
        img3.snapshotChanges()
      }
    })
  }
  receiverParcels() {
    const checkState0 = this.tracking.every(check => check.check < 1);
    const checkState1 = this.tracking.every(check => check.check < 2);
    if (this.tracking.length !== 0) {
      if (this.name !== "") {
        if (checkState0 === true) {
          this.CU_firebase();
          setTimeout(() => {
            this.snackBar.openFromComponent(PopUpSuccessComponent, {
              horizontalPosition: 'right',
              verticalPosition: 'top',
              duration: 5000,
              panelClass: ['custom-css-class']
            });
            this.tracking = [];
            location.reload();
          }, 300)
          setTimeout(() => {
            location.reload();
          }, 2000)
        } else if (checkState1 === true) {
          this.CU_firebase();
          setTimeout(() => {
            this.uploadImage()
            this.snackBar.openFromComponent(PopUpSuccessComponent, {
              horizontalPosition: 'right',
              verticalPosition: 'top',
              duration: 5000,
              panelClass: ['custom-css-class']
            });
            this.tracking = [];
            this.add_data();
          }, 300)
          setTimeout(() => {
            location.reload();
          }, 2000)
        } else if (checkState0 === false && checkState1 === false) {
          alert('มีรายการพัสดุ ที่ไม่ตรงกับชื่อเจ้าของพัสดุอยู่');
        }
        this.modalService.dismissAll();
      } else {
        alert('กรุณากรอกชื่อเจ้าของพัสดุ');
      }
    } else {
      alert('กรุณาเพิ่มหมายเลขติดตามพัสดุ');
    }
  }
}
@Component({
  selector: 'pop-up-success',
  template: `<span >
              <img style="width:3vw" src="../../../assets/images/box.svg" alt="">
              <strong class="ms-5">ส่งการแจ้งเตือนพัสดุเรียบร้อย</strong>
              <button type="button" class="btn float-right" (click)="closeSnackBar()">X</button>
            </span>`,
  styles: [``]
})
export class PopUpSuccessComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
@Component({
  selector: 'img-preview',
  template: ` <div class="modal-header">
  <div>
    <h4 class="modal-title">{{track}}</h4>
  </div>
  <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="d-flex align-items-center">
      <input type="button" value="" type="file" accept="image/*" #file0 hidden (change)="showImage0($event)">
      <p role="button" class="text-center border rounded h2 ps-2 pe-2 addImg" (click)="file0.click()" *ngIf="path0 === ''">+</p>
      <img role="button" *ngIf="path0 !== ''" [src]="path0" class="w-25 border rounded" alt="" (click)="file0.click()">

      <input type="button" value="" type="file" accept="image/*"  #file1 hidden (change)="showImage1($event)">
      <p role="button" class="text-center border rounded h2 ms-2 ps-2 pe-2 addImg" (click)="file1.click()" *ngIf="path1 === ''">+</p>
      <img role="button" *ngIf="path1 !== ''" [src]="path1" class="w-25 border rounded" alt="" (click)="file1.click()">

      <input type="button" value="" type="file" accept="image/*"  #file2 hidden  (change)="showImage2($event)">
      <p role="button" class="text-center border rounded h2 ms-2 ps-2 pe-2 addImg" (click)="file2.click()" *ngIf="path2 === ''">+</p>
      <img role="button" *ngIf="path2 !== ''" [src]="path2" class="w-25 border rounded" alt="" (click)="file2.click()">

      <input type="button" value="" type="file" accept="image/*"  #file3 hidden  (change)="showImage3($event)">
      <p role="button" class="text-center border rounded h2 ms-2 ps-2 pe-2 addImg" (click)="file3.click()" *ngIf="path3 === ''">+</p>
      <img role="button" *ngIf="path3 !== ''" [src]="path3" class="w-25 border rounded" alt="" (click)="file3.click()">
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-dark" (click)="passBack()">Close</button>
</div>`,
  styles: [`.addImg{background-color: #F0F2F5}`]
})

export class AddImageComponent {
  @Input() public track: string = '';
  @Input() public img0: string = '';
  @Input() public img1: string = '';
  @Input() public img2: string = '';
  @Input() public img3: string = '';
  @Input() public path_img0: any[] = [];
  @Input() public path_img1: any[] = [];
  @Input() public path_img2: any[] = [];
  @Input() public path_img3: any[] = [];
  path0: String = '';
  path1: String = '';
  path2: String = '';
  path3: String = '';
  constructor(public activeModal: NgbActiveModal, public sanitizer: DomSanitizer) {
  }
  ngOnInit(): void {
    if (this.path_img0 !== null) {
      this.showImage0(this.path_img0);
    }
    if (this.path_img1 !== null) {
      this.showImage1(this.path_img1);
    }
    if (this.path_img2 !== null) {
      this.showImage2(this.path_img2);
    }
    if (this.path_img3 !== null) {
      this.showImage3(this.path_img3);
    }
  }
  showImage0(e: any) {
    const target = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      this.path0 = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(target)) as string;
      this.img0 = target.name;
      this.path_img0 = e
    }
  }
  showImage1(e: any) {
    const target = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      this.path1 = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(target)) as string;
      this.img1 = target.name;
      this.path_img1 = e
    }
  }
  showImage2(e: any) {
    const target = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      this.path2 = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(target)) as string;
      this.img2 = target.name;
      this.path_img2 = e
    }
  }
  showImage3(e: any) {
    const target = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      this.path3 = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(target)) as string;
      this.img3 = target.name;
      this.path_img3 = e
    }
  }
  passBack() {
    const data = {
      tracking: this.track,
      img0: this.img0,
      img1: this.img1,
      img2: this.img2,
      img3: this.img3,
      path0: this.path_img0,
      path1: this.path_img1,
      path2: this.path_img2,
      path3: this.path_img3
    }
    this.activeModal.close(data);
  }
}
