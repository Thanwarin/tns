import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { EChartsOption } from 'echarts';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { DateModel, YearsModel } from 'src/app/@common/model/date';
import { UsersModel } from 'src/app/@common/model/users';

@Component({
  selector: 'app-admin-dashboard-statistics',
  templateUrl: './admin-dashboard-statistics.component.html',
  styleUrls: ['./admin-dashboard-statistics.component.scss']
})
export class AdminDashboardStatisticsComponent implements OnInit {
  year: string = ''
  after_years: ((startYear: any) => void) | undefined;
  all_years: any[] = []
  date: DateModel[] = [];
  status_dashboard: boolean = false;
  option: EChartsOption = {
    title: {
      text: 'สถิติการรับพัสดุในปี ' + this.year,
      subtext: 'จำนวนพัสดุ',
      subtextStyle: {
        fontSize: 16
      }
    },
    xAxis: {
      type: 'category',
      data: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม '],
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        type: 'bar',
        label: {
          show: true,
          position: 'inside'
        },
        itemStyle: {
          color: '#30009C',
        }
      }
    ],
    textStyle: {
      fontSize: 16,
      fontWeight: 5
    }
  };
  parcels$: Observable<any[]>;
  users$: Observable<any[]>;
  email: string = '';
  name: string = '';
  box_quantity = [];
  years: YearsModel[] = []
  Jan: number = 0;
  Feb: number = 0;
  Mar: number = 0;
  Apr: number = 0;
  May: number = 0;
  Jun: number = 0;
  Jul: number = 0;
  Aug: number = 0;
  Sep: number = 0;
  Oct: number = 0;
  Nov: number = 0;
  Dec: number = 0;
  min_average: number = 0;
  max_average: number = 0;
  year_minAverage: string = '-';
  year_maxAverage: string = '-';
  min_years: any[] = [];
  max_years: any[] = [];
  parcels_total: number = 0;
  count: any[] = []
  average_parcels = 0;


  constructor(private aft: AngularFirestore) {
    this.parcels$ = aft.collection('parcels').valueChanges();
    this.users$ = aft.collection('users').valueChanges();
  }

  ngOnInit(): void {
    const date = new Date();
    const current_date = date.toLocaleDateString('en-US', {
      year: 'numeric',
    });
    this.year = current_date;
    this.after_years = function (startYear) {
      const current_years = new Date().getFullYear(), years: any[] = [];
      startYear = startYear || 1980;
      while (startYear <= current_years) {
        years.push(startYear++);
      } return years;
    }
    const all_years = this.after_years(date.getFullYear() - 5);
    this.all_years.push(all_years);
    this.all_years.forEach(year => {
      year.forEach((y: any) => {
        this.years.push(
          {
            y,
            month: [{
              Jan: [],
              Feb: [],
              Mar: [],
              Apr: [],
              May: [],
              Jun: [],
              Jul: [],
              Aug: [],
              Sep: [],
              Oct: [],
              Nov: [],
              Dec: []
            }]
          }
        )
      })
    })
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(tk_email => {
      this.email = tk_email.email;
    });
    this.users$.subscribe((data) => {
      data.forEach((users: UsersModel) => {
        if (users.email === this.email) {
          this.name = `${users.firstname}` + `${users.lastname}`;
        }
      })
    })
    this.parcels$.subscribe((data) => {
      data.forEach((parcel) => {
        const owner = `${parcel.owner?.firstname}` + `${parcel.owner?.lastname}`;
        const receiver = `${parcel.receiver?.firstname}` + `${parcel.receiver?.lastname}`;
        // if (owner === this.name || receiver === this.name) {
        if (parcel.checkOut !== null) {
          const date_CheckOut = new Date(`${parcel.checkOut}`);
          const date = date_CheckOut.toLocaleDateString('en-US', {
            year: 'numeric',
            month: 'short'
          });
          const year = date_CheckOut.toLocaleDateString('en-US', {
            year: 'numeric',
          });
          const month = date_CheckOut.toLocaleDateString('en-US', {
            month: 'short'
          });
          const all_date = {
            date: date,
            year: year,
            month: month
          }
          this.date.push(all_date);
        }
      })
    })
  }
  select_year(year: string) {
    this.status_dashboard = true;
    this.Jan = 0;
    this.Feb = 0;
    this.Mar = 0;
    this.Apr = 0;
    this.May = 0;
    this.Jun = 0;
    this.Jul = 0;
    this.Aug = 0;
    this.Sep = 0;
    this.Oct = 0;
    this.Nov = 0;
    this.Dec = 0;
    this.years = [{
      y: '',
      month: [{
        Jan: [],
        Feb: [],
        Mar: [],
        Apr: [],
        May: [],
        Jun: [],
        Jul: [],
        Aug: [],
        Sep: [],
        Oct: [],
        Nov: [],
        Dec: []
      }]
    }];
    const get_year = `${year}`;
    this.year = get_year;
    this.date.forEach((data) => {

      if (data.year === this.year) {
        this.years.forEach((y) => {
          y.y = this.year;
          if (data.month === 'Jan') {
            y.month?.[0].Jan?.push(data.month)
          } else if (data.month === 'Feb') {
            y.month?.[0].Feb?.push(data.month)
          } else if (data.month === 'Mar') {
            y.month?.[0].Mar?.push(data.month)
          } else if (data.month === 'Apr') {
            y.month?.[0].Apr?.push(data.month)
          } else if (data.month === 'May') {
            y.month?.[0].May?.push(data.month)
          } else if (data.month === 'Jun') {
            y.month?.[0].Jun?.push(data.month)
          } else if (data.month === 'Jul') {
            y.month?.[0].Jul?.push(data.month)
          } else if (data.month === 'Aug') {
            y.month?.[0].Aug?.push(data.month)
          } else if (data.month === 'Sep') {
            y.month?.[0].Sep?.push(data.month)
          } else if (data.month === 'Oct') {
            y.month?.[0].Oct?.push(data.month)
          } else if (data.month === 'Nov') {
            y.month?.[0].Nov?.push(data.month)
          } else if (data.month === 'Dec') {
            y.month?.[0].Dec?.push(data.month)
          }
        })
      }
    })
    if (this.years[0].month?.[0].Jan?.length === 0) {
      this.Jan = parseInt(`${this.years[0].month?.[0].Jan?.length}`);
    } else {
      this.Jan = parseInt(`${this.years[0].month?.[0].Jan?.length}`);
    }

    if (this.years[0].month?.[0].Feb?.length === 0) {
      this.Feb = parseInt(`${this.years[0].month?.[0].Feb?.length}`);
    } else {
      this.Feb = parseInt(`${this.years[0].month?.[0].Feb?.length}`);
    }

    if (this.years[0].month?.[0].Mar?.length === 0) {
      this.Mar = parseInt(`${this.years[0].month?.[0].Mar?.length}`);
    } else {
      this.Mar = parseInt(`${this.years[0].month?.[0].Mar?.length}`);
    }

    if (this.years[0].month?.[0].Apr?.length === 0) {
      this.Apr = parseInt(`${this.years[0].month?.[0].Apr?.length}`);
    } else {
      this.Apr = parseInt(`${this.years[0].month?.[0].Apr?.length}`);
    }

    if (this.years[0].month?.[0].May?.length === 0) {
      this.May = parseInt(`${this.years[0].month?.[0].May?.length}`);
    } else {
      this.May = parseInt(`${this.years[0].month?.[0].May?.length}`);
    }

    if (this.years[0].month?.[0].Jun?.length === 0) {
      this.Jun = parseInt(`${this.years[0].month?.[0].Jun?.length}`);
    } else {
      this.Jun = parseInt(`${this.years[0].month?.[0].Jun?.length}`);
    }

    if (this.years[0].month?.[0].Jul?.length === 0) {
      this.Jul = parseInt(`${this.years[0].month?.[0].Jul?.length}`);
    } else {
      this.Jul = parseInt(`${this.years[0].month?.[0].Jul?.length}`);
    }

    if (this.years[0].month?.[0].Aug?.length === 0) {
      this.Aug = parseInt(`${this.years[0].month?.[0].Aug?.length}`);
    } else {
      this.Aug = parseInt(`${this.years[0].month?.[0].Aug?.length}`);
    }

    if (this.years[0].month?.[0].Sep?.length === 0) {
      this.Sep = parseInt(`${this.years[0].month?.[0].Sep?.length}`);
    } else {
      this.Sep = parseInt(`${this.years[0].month?.[0].Sep?.length}`);
    }

    if (this.years[0].month?.[0].Oct?.length === 0) {
      this.Oct = parseInt(`${this.years[0].month?.[0].Oct?.length}`);
    } else {
      this.Oct = parseInt(`${this.years[0].month?.[0].Oct?.length}`);
    }

    if (this.years[0].month?.[0].Nov?.length === 0) {
      this.Nov = parseInt(`${this.years[0].month?.[0].Nov?.length}`);
    } else {
      this.Nov = parseInt(`${this.years[0].month?.[0].Nov?.length}`);
    }

    if (this.years[0].month?.[0].Dec?.length === 0) {
      this.Dec = parseInt(`${this.years[0].month?.[0].Dec?.length}`);
    } else {
      this.Dec = parseInt(`${this.years[0].month?.[0].Dec?.length}`);
    }
    const count_years = [
      { year: 'มกราคม', total: this.Jan },
      { year: 'กุมภาพันธ์', total: this.Feb },
      { year: 'มีนามคม', total: this.Mar },
      { year: 'เมษายน', total: this.Apr },
      { year: 'พฤษภาคม', total: this.May },
      { year: 'มิถุนายน', total: this.Jun },
      { year: 'กรกฎาคม', total: this.Jul },
      { year: 'สิงหาคม', total: this.Aug },
      { year: 'กันยายน', total: this.Sep },
      { year: 'ตุลาคม', total: this.Oct },
      { year: 'พฤษจิกายน', total: this.Nov },
      { year: 'ธันวาคม', total: this.Dec },
    ];
    this.count = [];
    count_years.forEach((data) => {
      this.count.push(data.total);
    })
    const get_min = () => {
      return Math.min.apply(null, this.count)
    }
    const get_max = () => {
      return Math.max.apply(null, this.count)
    }
    count_years.forEach((data) => {
      if (data.total === get_min()) {
        this.min_average = data.total;
        this.min_years.push(data.year);
      }
      if (data.total === get_max()) {
        this.max_average = data.total;
        this.max_years.push(data.year);
      }
    })
    this.year_minAverage = this.min_years[0];
    this.year_maxAverage = this.max_years[0];
    this.parcels_total = this.Jan + this.Feb + this.Mar + this.Apr + this.May + this.Jun + this.Jul + this.Aug + this.Sep + this.Oct + this.Nov + this.Dec;
    this.average_parcels = parseInt(`${this.parcels_total / 12}`);
    this.year = `${parseInt(year) + 543}`;
    this.option = {
      title: {
        text: 'สถิติการรับพัสดุในปี ' + this.year,
        subtext: 'จำนวนพัสดุ',
        subtextStyle: {
          fontSize: 16
        }
      },
      xAxis: {
        type: 'category',
        data: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม '],
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: [this.Jan, this.Feb, this.Mar, this.Apr, this.May, this.Jun, this.Jul, this.Aug, this.Sep, this.Oct, this.Nov, this.Dec],
          type: 'bar',
          label: {
            show: true,
            position: 'inside'
          },
          itemStyle: {
            color: '#30009C',
          }
        }
      ],
      textStyle: {
        fontSize: 16,
        fontWeight: 5
      }
    }
  }

}
