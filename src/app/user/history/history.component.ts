import { Component, OnInit, AfterViewInit, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, AfterViewInit {
  listWaitParcels$: Observable<any[]>;
  statusParcels: boolean = false;
  statusPage: number = 0;
  list_Parcel0: any[] = [];
  list_Parcel1: any[] = [];
  users$: Observable<any[]>;
  owner: any[] = [];
  receiver: any[] = [];
  trackNumber: any[] = [];
  timeStamp_checkIn: string[] = [];
  timeStamp_checkOut: string[] = [];
  ownerName: string = '';
  search_Name: EventEmitter<string> = new EventEmitter();
  stateSearch: number = 1;
  selectSearch: string = 'ผู้รับพัสดุ';
  owner_name = '';
  receiverName = '';
  track = '';
  date_checkIn = '';
  date_checkOut = '';
  owner_Filter: string = '';
  receiver_Filter: string = '';
  track_Filter: string = '';
  date_checkIn_Filter: string = '';
  date_checkOut_Filter: string = '';
  constructor(private firestore: AngularFirestore) {
    this.listWaitParcels$ = firestore.collection('parcels', ref => ref.orderBy('checkIn', 'asc')).valueChanges({ idField: 'ID' });
    this.users$ = firestore.collection('users').valueChanges();
  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.search();
    }, 300)
  }
  changeState_search(state: number, select: string) {
    this.stateSearch = state
    this.selectSearch = select;
  }
  resetForm() {
    this.searchName('');
    this.searchReceiver('');
    this.searchTrack('');
    this.searchDate_checkIn('');
    this.searchDate_checkOut('');
  }
  filterItems(arr: any[], query: string) {
    return arr.filter(function (el) {
      return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
  }
  searchName(name: string) {
    this.search_Name.emit(name);
    this.owner_name = name;
    this.search();
  }
  searchReceiver(receiver: string) {
    this.search_Name.emit(receiver);
    this.receiverName = receiver;
    this.search();
  }
  searchTrack(track: string) {
    this.search_Name.emit(track);
    this.track = track;
    this.search();
  }
  searchDate_checkIn(date: string) {
    this.search_Name.emit(date);
    let dateTime = new Date(date),
      month = '' + (dateTime.getMonth() + 1),
      day = '' + dateTime.getDate(),
      year = '' + dateTime.getFullYear();
    this.date_checkIn = date;
    this.search();
    return [year, month, day].join('-');
  }
  searchDate_checkOut(date: string) {
    this.search_Name.emit(date);
    let dateTime = new Date(date),
      month = '' + (dateTime.getMonth() + 1),
      day = '' + dateTime.getDate(),
      year = '' + dateTime.getFullYear();
    this.date_checkOut = date;
    this.search();
    return [year, month, day].join('-');
  }
  search() {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    const email = token.forEach((token) => {
      this.users$.subscribe((user) => {
        user.forEach(data_user => {
          if (data_user.email === token.email) {
            this.ownerName = data_user.firstname + ' ' + data_user.lastname;
          }
        })
      })
    });
    this.listWaitParcels$.subscribe(data => { return this.list_Parcel0 = [], this.list_Parcel1 = [] })
    this.listWaitParcels$.subscribe(dataFromFireStore => {
      dataFromFireStore.forEach(item => {
        const item_owner = item.owner.firstname + item.owner.lastname;
        const item_receiver = item.receiver.firstname + item.receiver.lastname;
        this.owner.push(item_owner);
        this.receiver.push(item_receiver);
        this.trackNumber.push(item.trackNumber);
        if (item.checkOut !== null && item.owner.firstname + ' ' + item.owner.lastname === this.ownerName) {
          const setDate = (itemD: any) => {
            let dateTime = new Date(itemD),
              month = '' + (dateTime.getMonth() + 1),
              day = '' + dateTime.getDate(),
              year = '' + dateTime.getFullYear();
            if (month.length < 2) {
              month = '0' + month;
            }
            if (day.length < 2) {
              day = '0' + day;
            }
            return [year, month, day].join('-');
          };
          const checkIn_date = setDate(item.checkIn);
          this.timeStamp_checkIn.push(checkIn_date);
          const checkOut_date = setDate(item.checkOut);
          this.timeStamp_checkOut.push(checkOut_date);
          const filter_name = this.filterItems(this.owner, this.owner_name);
          filter_name.forEach((filter) => {
            let name = '';
            for (let i of filter) {
              name += i;
            }
            this.owner_Filter = name;
          });

          const filter_receiver = this.filterItems(this.receiver, this.receiverName);
          filter_receiver.forEach((filter) => {
            let receiver = '';
            for (let i of filter) {
              receiver += i;
            }
            this.receiver_Filter = receiver
          });

          const filter_track = this.filterItems(this.trackNumber, this.track);
          filter_track.forEach((filter) => {
            let track = '';
            for (let i of filter) {
              track += i;
            }
            this.track_Filter = track;
          });

          const filter_date_checkIn = this.filterItems(this.timeStamp_checkIn, this.date_checkIn);
          filter_date_checkIn.forEach((filter) => {
            let date = '';
            for (let i of filter) {
              date += i;
            }
            this.date_checkIn_Filter = date;
          });

          const filter_date_checkOut = this.filterItems(this.timeStamp_checkOut, this.date_checkOut);
          filter_date_checkOut.forEach((filter) => {
            let date = '';
            for (let i of filter) {
              date += i;
            }
            this.date_checkOut_Filter = date;
          });

          if (item_owner === this.owner_Filter &&
            item_receiver === this.receiver_Filter &&
            item.trackNumber === this.track_Filter &&
            checkIn_date === this.date_checkIn_Filter &&
            checkOut_date === this.date_checkOut_Filter) {
            this.list_Parcel0.push(item);
            this.statusParcels = true;
          }
        } else {
          this.list_Parcel1.push(item);
        }
      });
    })
  }
  activeStatus(p: number) {
    this.statusPage = p;
  }
}
