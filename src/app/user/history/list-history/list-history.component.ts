import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { StatusParcelsModel } from 'src/app/@common/enums/statusParcels.enum';
import { ParcelsModel } from 'src/app/@common/model/parcels';

@Component({
  selector: 'app-list-history',
  templateUrl: './list-history.component.html',
  styleUrls: ['./list-history.component.scss']
})
export class ListHistoryComponent implements OnInit {
  @Input() listParcels: ParcelsModel = {};
  @Input() statusParcels: StatusParcelsModel = 0;
  listImage!: { Img0: Observable<any>; Img1: Observable<any>; Img2: Observable<any>; Img3: Observable<any>; };
  time = '';
  date_checkIn = '';
  date_checkOut = '';
  email: string = '';
  ownerName: string = '';
  name: string = ''
  storage!: Observable<string | null>;
  constructor(public angularstorage: AngularFireStorage) { }

  ngOnInit(): void {
    const Img0 = this.angularstorage.ref(`${this.listParcels.images?.image0}`).getDownloadURL();
    const Img1 = this.angularstorage.ref(`${this.listParcels.images?.image1}`).getDownloadURL();
    const Img2 = this.angularstorage.ref(`${this.listParcels.images?.image2}`).getDownloadURL();
    const Img3 = this.angularstorage.ref(`${this.listParcels.images?.image3}`).getDownloadURL();
    const listImg = { Img0, Img1, Img2, Img3 };
    this.listImage = listImg;
    const checkIn = new Date(`${this.listParcels.checkIn}`);
    const result_checkIn = checkIn.toLocaleTimeString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: '2-digit',
      minute: 'numeric',
    });
    this.date_checkIn = result_checkIn;
    const checkOut = new Date(`${this.listParcels.checkOut}`);
    const result_checkOut = checkOut.toLocaleTimeString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: '2-digit',
      minute: 'numeric',
    });
    this.date_checkOut = result_checkOut;
  }

}