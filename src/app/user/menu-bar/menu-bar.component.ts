import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { statusPage } from 'src/app/@common/enums/status-page.enum';
import { UsersModel } from 'src/app/@common/model/users';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {
  statusPage: string = '';
  users$: Observable<any[]>;
  imageProfile!: Observable<any>;
  statusProfile: boolean = false;
  firstname: string = '';
  lastname: string = '';
  count_messages: number = 0;
  length_messages: any[] = [];
  id_messages: any[] = [];
  messages$: Observable<any[]>;
  @Output() page = new EventEmitter<string>();
  outline = 'material-icons-outlined';
  constructor(private auth: AuthService, private firestore: AngularFirestore, private angularstorage: AngularFireStorage) {
    this.users$ = firestore.collection('users').valueChanges();
    this.messages$ = firestore.collection('messages').valueChanges({ idField: 'ID' });
  }
  ngOnInit(): void {
    const page = sessionStorage.getItem('page');
    this.statusPage = `${page}`
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    const email = token.forEach((token) => {
      this.users$.subscribe(data => {
        data.forEach((users) => {
          if (users.email === token.email) {
            this.firstname = users.firstname;
            this.lastname = users.lastname;
            if (users.imageProfile !== '') {
              const image = this.angularstorage.ref(`${users.imageProfile}`).getDownloadURL();
              this.imageProfile! = image;
              this.statusProfile = true;
            } else {
              this.statusProfile = false;
            }
          }
        })
      })
    })
    this.messages$.subscribe((data) => {
      this.length_messages = [];
      this.id_messages = [];
      data.forEach((messages) => {
        if (messages.read === false && messages.receiver.firstname === this.firstname && messages.receiver.lastname === this.lastname) {
          this.length_messages.push(messages)
          this.count_messages = this.length_messages.length;
          this.id_messages.push(messages.ID);
        }
      })
    })
  }
  read_message() {
    if (this.count_messages !== 0) {
      this.id_messages.forEach(id => {
        this.firestore.collection('messages').doc(id).update({
          read: true
        }).then(() => this.count_messages = 0)
      })
    }
  }
  goTo(p: string) {
    if (p === 'home-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'add-receiver-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'edit-profile-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'history-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'dashboard-statistics-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'scan-page') {
      sessionStorage.setItem('page', p);
    } else if (p === 'notification-page') {
      sessionStorage.setItem('page', p);
    }
    this.statusPage = p;
    this.page.emit(p);
  }
  logOut() {
    this.auth.logout();
  }
} 