import { Component, EventEmitter, Input, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { UsersModel } from 'src/app/@common/model/users';
import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-add-receiver',
  templateUrl: './add-receiver.component.html',
  styleUrls: ['./add-receiver.component.scss']
})
export class AddReceiverComponent implements OnInit {
  @ViewChild('content') private content: any;
  list_User$: Observable<any[]>
  list_users: any[] = [];
  list: any[] = [];
  list_Parcels$: Observable<any[]>
  list_parcels: any[] = [];
  getSearch: string = '';
  name: string = '';
  email = '';
  list_agent: any[] = [];
  agent: any[] = [];
  agent0: string = '';
  agent1: string = '';
  agent2: string = '';
  id = '';
  isChecked = false;
  data_delete: any[] = [];
  token: any;
  name_user: string = '';
  data_users: UsersModel[] = [];
  firstname: string = '';
  lastname: string = '';
  receiver_fname: string = '';
  receiver_lname: string = '';
  status_update = '';
  count_agent: number = 0;
  check: EventEmitter<string> = new EventEmitter();
  constructor(public snackBar: MatSnackBar, private afMessaging: AngularFireMessaging, private Afs: AngularFirestore, private modalService: NgbModal, private mgs: MessagingService, private auth: AngularFireAuth) {
    this.list_User$ = Afs.collection('users').valueChanges({ idField: 'ID' });
    this.list_Parcels$ = Afs.collection('parcels').valueChanges();
  }

  ngOnInit(): void {
    this.afMessaging.requestToken.subscribe((token) => {
      this.token = token;
    },
      (err) => { console.log(err) });
    this.list_Parcels$.subscribe(data => {
      data.forEach(listParcels => {
        this.list_parcels.push(listParcels);
      });
    })
    const session: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(session));
    token.forEach((token) => {
      this.email = token.email;
    })
    this.list_User$.subscribe(data => {
      data.forEach(user => {
        if (this.email === user.email) {
          this.firstname = user.firstname;
          this.lastname = user.lastname;
          this.name_user = user.firstname + ' ' + user.lastname;
          this.id = user.ID;
          this.agent0 = user.agent.agent0.firstname + user.agent.agent0.lastname;
          this.agent1 = user.agent.agent1.firstname + user.agent.agent1.lastname;
          this.agent2 = user.agent.agent2.firstname + user.agent.agent2.lastname;
          const agent = [user.agent.agent0, user.agent.agent1, user.agent.agent2];
          this.agent = agent;
          this.list_agent = user.agent;
          agent.forEach(ag => {
            if (ag.firstname + ag.lastname !== '') {
              const count = [ag];
              this.count_agent = count.length;
            }
          })
        }
      });
    });
    this.list_User$.subscribe(data => {
      data.forEach(list => {
        if (list.firstname !== this.firstname && list.lastname !== this.lastname) {
          this.list.push(list.firstname + ' ' + list.lastname);
          this.data_users.push(list);
          const remove = (arr: any) => [...new Set(arr)];
          this.list_users = remove(this.list);
        }
      });
    })
  }

  check_Parcel() {
    this.list_parcels.forEach(item => {
      if (item.owner.firstname + ' ' + item.owner.lastname === this.name) {
      }
    })
  }
  add_agent() {
    this.data_users.forEach((data) => {
      if (data.firstname + ' ' + data.lastname === this.name) {
        this.receiver_fname = `${data.firstname}`;
        this.receiver_lname = `${data.lastname}`;
      }
    });
    const modalRef = this.modalService.open(PopupAddReceiverComponent);
    const sendValue = modalRef.componentInstance;
    sendValue.name_receiver = this.name;
    sendValue.receiver_fname = this.receiver_fname;
    sendValue.receiver_lname = this.receiver_lname;
    sendValue.firstname = this.firstname;
    sendValue.lastname = this.lastname;
  }
  cancel() {
    this.modalService.dismissAll()
  }
  open_Modal(data: any) {
    this.data_delete = [];
    this.data_delete.push(data);
    this.modalService.open(this.content);
  }
  delete_agent() {
    const ag0 = this.agent0;
    const ag1 = this.agent1;
    const ag2 = this.agent2;
    const check = this.data_delete.forEach((data: any) => {
      const data_name = data.firstname + data.lastname;
      if (data_name === ag0) {
        this.CRUD_firebase(0, '', '', false)
      } else if (data_name === ag1) {
        this.CRUD_firebase(1, '', '', false)
      } else if (data_name === ag2) {
        this.CRUD_firebase(2, '', '', false)
      }
    })
    this.snackBar.openFromComponent(PopUpRemoveReceiverComponent, {
      horizontalPosition: 'right',
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['custom-css-class'],
    });
  }
  CRUD_firebase(agent_count: number, fname: string, lname: string, status: boolean,
  ) {
    const remove = (arr: any) => [...new Set(arr)];
    const result = remove([this.list_agent]);
    result.forEach((agent: any) => {
      if (agent_count === 0) {
        this.Afs.collection('users').doc(this.id).update({
          agent: {
            agent0: {
              firstname: fname,
              lastname: lname,
              status_receiver: status,
            },
            agent1: {
              firstname: agent.agent1.firstname,
              lastname: agent.agent1.lastname,
              status_receiver: agent.agent1.status_receiver,
            },
            agent2: {
              firstname: agent.agent2.firstname,
              lastname: agent.agent2.lastname,
              status_receiver: agent.agent2.status_receiver,
            }
          }
        })
      } else if (agent_count === 1) {
        this.Afs.collection('users').doc(this.id).update({
          agent: {
            agent0: {
              firstname: agent.agent0.firstname,
              lastname: agent.agent0.lastname,
              status_receiver: agent.agent0.status_receiver,
            },
            agent1: {
              firstname: fname,
              lastname: lname,
              status_receiver: status,
            },
            agent2: {
              firstname: agent.agent2.firstname,
              lastname: agent.agent2.lastname,
              status_receiver: agent.agent2.status_receiver,
            }
          }
        })
      } else if (agent_count === 2) {
        this.Afs.collection('users').doc(this.id).update({
          agent: {
            agent0: {
              firstname: agent.agent0.firstname,
              lastname: agent.agent0.lastname,
              status_receiver: agent.agent0.status_receiver,
            },
            agent1: {
              firstname: agent.agent1.firstname,
              lastname: agent.agent1.lastname,
              status_receiver: agent.agent1.status_receiver,
            },
            agent2: {
              firstname: fname,
              lastname: lname,
              status_receiver: status,
            }
          }
        })
      }
    })
  }
  notification(event: any, fname: string, lname: string) {
    const name = fname + lname;
    const remove = (arr: any) => [...new Set(arr)];
    const result = remove([this.list_agent]);
    result.forEach((agent: any) => {
      if (name === this.agent0) {
        this.CRUD_firebase(0, fname, lname, event.checked)
      } else if (name === this.agent1) {
        this.CRUD_firebase(1, fname, lname, event.checked)
      } else if (name === this.agent2) {
        this.CRUD_firebase(2, fname, lname, event.checked)
      }
    })
  }

  search(e: string) {
    this.check.emit(e)
    if (this.check) {
      this.check_Parcel()
    }
  }
}
@Component({
  selector: 'popup-add-receiver',
  template: `<div class="p-3">
    <div class="modal-title">
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="activeModal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-header border-0">
        
    </div>
    <div class="modal-body">
    <div class="d-flex justify-content-center">
          <h3>เพิ่มผู้รับพัสดุ</h3>
        </div>
      <div class="d-flex justify-content-center mt-4">
        <p>{{name_receiver}}</p>
      </div>
      <div class="d-flex  justify-content-center mt-3">
        <button class="me-2" mat-flat-button (click)="activeModal.dismiss('Cross click')">
          ยกเลิก
        </button>
        <button
          class="btnedit ms-2"
          mat-flat-button
          (click)="add_receiver()"
          (click)="activeModal.dismiss('Cross click')"
        >
          ยืนยัน
        </button>
      </div>

    </div>
  </div>`,
  styles: [
    `
    .btnedit{
        background-color: #30009C;
        color: white;
    }
}`,
  ],
})
export class PopupAddReceiverComponent {
  @Input() public name_receiver: string = '';
  @Input() public receiver_fname: string = '';
  @Input() public receiver_lname: string = '';
  @Input() public firstname: string = '';
  @Input() public lastname: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
  }
  ngOnInit(): void {
  }
  add_receiver() {
    try {

      this.aft.collection('messages').add({
        data: {
          trackNumber: '',
          receiver_parcel: {
            firstname: '',
            lastname: ''
          }
        },
        dateTime: Date(),
        read: false,
        receiver: {
          firstname: this.receiver_fname,
          lastname: this.receiver_lname
        },
        sender: {
          firstname: this.firstname,
          lastname: this.lastname
        },
        type: 1
      }).then(() => {
        this.snackBar.openFromComponent(PopUpWaitReceiverComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
      })
    } catch (err) {
      alert('เพิ่มผู้รับแทนไม่สำเร็จ');
    }
  }
}
@Component({
  selector: 'wait-reciver',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-warning" style="font-size: 37px;">hourglass_top</mat-icon>
    <strong class="ms-5">รอการตอบรับพัสดุแทน</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpWaitReceiverComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}

@Component({
  selector: 'remove-reciver',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-success" style="font-size: 37px;">check_circle</mat-icon>
    <strong class="ms-5">ลบผู้รับพัสดุแทนเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpRemoveReceiverComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}

