import { Component, OnInit, SimpleChange, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { ParcelsModel } from 'src/app/@common/model/parcels';
import { formatsAvailable } from 'src/app/barcode-formats';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.scss'],
})
export class ScanComponent implements OnInit {
  @ViewChild('content') private content: any;
  formats = formatsAvailable;
  openCamera = true;
  tracking: any[] = [];
  device: any;
  devices: MediaDeviceInfo[] = [];
  result: Array<string> = [];
  tryHarder = false;
  dbParcels$: Observable<any[]>;
  availableDevices!: MediaDeviceInfo[];
  currentDevice!: MediaDeviceInfo | null;
  hasDevices!: boolean;
  hasPermission!: boolean;
  list_trackNumber: Array<string> = [];
  list_users$: Observable<any[]>;
  data_User: any[] = [];
  email: string = '';
  list_Tracking = '';
  checkTrack: Array<number> = [];
  agent0: string = '';
  agent1: string = '';
  agent2: string = '';
  i: number = 0;
  firstname: string = '';
  lastname: string = '';
  assignor: string = '';
  parcels: ParcelsModel[] = [];
  fname_owner: string = '';
  lname_owner: string = '';
  email_owner: string = '';
  ag0_fname: string = '';
  ag0_lname: string = '';
  ag0_email: string = '';
  status_ag0: boolean = false;
  ag1_fname: string = '';
  ag1_lname: string = '';
  ag1_email: string = '';
  status_ag1: boolean = false;
  ag2_fname: string = '';
  ag2_lname: string = '';
  ag2_email: string = '';
  status_ag2: boolean = false;
  parcel: any[] = [];
  users: any[] = [];
  list_assign_users: any[] = [];
  check_camera: boolean = false;
  constructor(private modalService: NgbModal, private snackBar: MatSnackBar, private aft: AngularFirestore) {
    this.dbParcels$ = aft.collection('parcels').valueChanges({ idField: 'ID' });
    this.list_users$ = aft.collection('users').valueChanges();
    this.list_users$.subscribe(data => {
      data.forEach(users => {
        this.users.push(users)
      })
    })
    try {
      this.currentDevice = null
    } catch (err) {
      console.log(err)
    }
  }

  ngOnInit(): void {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(tk_email => {
      this.email = tk_email.email;
    });
    this.list_users$.subscribe((data) => {
      this.list_assign_users = [];
      data.forEach((user) => {
        if (user.email === this.email) {
          this.firstname = `${user.firstname}`;
          this.lastname = `${user.lastname}`;
          this.data_User.push(user);
        }
      });
      data.forEach((user) => {
        this.list_assign_users.push(this.firstname + this.lastname);
        if (this.firstname === user.agent?.agent0.firstname && this.lastname === user.agent?.agent0.lastname) {
          this.list_assign_users.push(user.firstname + user.lastname);
        } else if (this.firstname === user.agent?.agent1.firstname && this.lastname === user.agent?.agent1.lastname) {
          this.list_assign_users.push(user.firstname + user.lastname);
        } else if (this.firstname === user.agent?.agent2.firstname && this.lastname === user.agent?.agent2.lastname) {
          this.list_assign_users.push(user.firstname + user.lastname);
        }
      })
    });
    this.dbParcels$.subscribe(data => {
      data.forEach((parcels) => {
        this.parcels.push(parcels);
        const name_owner = parcels.owner?.firstname + parcels.owner?.lastname;
        if (parcels.checkOut === null && parcels.checkIn !== null) {
          this.list_assign_users.forEach(data => {
            if (parcels.owner.firstname + parcels.owner.lastname === data) {
              this.list_trackNumber.push(`${parcels.trackNumber}`)
              console.log(this.list_trackNumber)
            }
          })
        }
      });
    });
  }
  ngOnChanges(changes: SimpleChange): void {
    this.modalService.open(this.content);
  }
  camerasFoundHandler(d: MediaDeviceInfo[]) {
    if (d.length > 0) {
      this.check_camera = true;
    }
    this.availableDevices = d;
    this.devices = d;
    this.i = this.devices.length - 1;
  }
  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }
  changeCamera(change: any) {
    const count = this.devices.length;
    if (this.i < count && this.i !== 0) {
      this.i--
    } else if (this.i === 0) {
      this.i = count - 1;
    }
    this.currentDevice = this.devices[this.i]
  }
  scanSuccessHandler(result: any) {
    try {
      this.list_Tracking = result;
      this.openPopUp(result);

    } catch (err) {
      console.log(err);
    }
  }
  cancel() {
    this.tracking = [];
  }
  openPopUp(message: string) {
    if (confirm('หมายเลขติดตามพัสดุ : ' + message)) {
      let check = 0;
      const result = this.list_trackNumber.find(data => data === this.list_Tracking)
      if (result === undefined) {
        check = 1;
      } else {
        check = 0
      }
      const check_list = {
        tracking: this.list_Tracking,
        check: check
      }
      const setArr = this.tracking.find(data => data.tracking === check_list.tracking)
      if (setArr === undefined) {
        this.tracking.push(check_list);
      } else {
      }
      this.modalService.open(this.content);
    } else {

    }
  }
  deleteTrack(track: any) {
    function arrayRemove(arr: any, value: any) {
      return arr.filter(function (ele: any) {
        return ele != value;
      });
    }
    const list_track = this.tracking.find(data => data.tracking === track)
    const result = arrayRemove(this.tracking, list_track);
    this.tracking = result;
  }
  addTracking(e: any) {
    this.modalService.dismissAll();
  }
  receiverParcels() {
    const checkState = this.tracking.every(check => check.check < 1);
    if (checkState === true) {
      this.parcels.forEach((parcels: any) => {
        this.tracking.forEach((list) => {
          if (list.tracking === parcels.trackNumber) {
            this.parcel.push(parcels);
            this.users.forEach(user => {
              if (user.firstname + user.lastname === parcels.owner.firstname + parcels.owner.lastname) {
                this.fname_owner = user.firstname; //เจ้าของพัสดุ
                this.lname_owner = user.lastname; //เจ้าของพัสดุ
                this.email_owner = user.email;
                this.ag0_fname = user.agent.agent0?.firstname;
                this.ag0_lname = user.agent.agent0?.lastname;
                this.ag0_email = user.agent.agent0?.email;
                this.status_ag0 = user.agent.agent0?.status_receiver;
                this.ag1_fname = user.agent.agent1?.firstname;
                this.ag1_lname = user.agent.agent1?.lastname;
                this.ag1_email = user.agent.agent1?.email;
                this.status_ag1 = user.agent.agent1?.status_receiver;
                this.ag2_fname = user.agent.agent2?.firstname;
                this.ag2_lname = user.agent.agent2?.lastname;
                this.ag2_email = user.agent.agent2?.email;
                this.status_ag2 = user.agent.agent2?.status_receiver;
                this.aft.collection('messages').add({ //ส่งให้เจ้าหน้าที่
                  data: {
                    receiver_parcel: {
                      firstname: this.firstname,
                      lastname: this.lastname
                    },
                    owner: {
                      firstname: this.fname_owner,
                      lastname: this.lname_owner
                    },
                    trackNumber: list.tracking
                  },
                  dateTime: Date(),
                  read: false,
                  receiver: {
                    firstname: 'administrator',
                    lastname: 'en01',
                  },
                  sender: {
                    firstname: this.firstname,
                    lastname: this.lastname
                  },
                  type: 0
                });
                this.aft.collection('messages').add({ //ส่งให้เจ้าของ
                  data: {
                    receiver_parcel: {
                      firstname: this.firstname,
                      lastname: this.lastname
                    },
                    owner: {
                      firstname: this.fname_owner,
                      lastname: this.lname_owner
                    },
                    trackNumber: list.tracking
                  },
                  dateTime: Date(),
                  read: false,
                  receiver: {
                    firstname: this.fname_owner,
                    lastname: this.lname_owner,
                    email: this.email_owner
                  },
                  sender: {
                    firstname: this.firstname,
                    lastname: this.lastname
                  },
                  type: 0
                });
                if (this.ag0_fname !== '' && this.ag0_lname !== '' && this.status_ag0 === true) {
                  this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 1
                    data: {
                      receiver_parcel: {
                        firstname: this.firstname,
                        lastname: this.lastname
                      },
                      owner: {
                        firstname: this.fname_owner,
                        lastname: this.lname_owner
                      },
                      trackNumber: list.tracking
                    },
                    dateTime: Date(),
                    read: false,
                    receiver: {
                      firstname: this.ag0_fname,
                      lastname: this.ag0_lname,
                      email: this.ag0_email
                    },
                    sender: {
                      firstname: this.firstname,
                      lastname: this.lastname
                    },
                    type: 0
                  });
                }
                if (this.ag1_fname !== '' && this.ag1_lname !== '' && this.status_ag1 === true) {
                  this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 2
                    data: {
                      receiver_parcel: {
                        firstname: this.firstname,
                        lastname: this.lastname
                      },
                      owner: {
                        firstname: this.fname_owner,
                        lastname: this.lname_owner
                      },
                      trackNumber: list.tracking
                    },
                    dateTime: Date(),
                    read: false,
                    receiver: {
                      firstname: this.ag1_fname,
                      lastname: this.ag1_lname,
                      email: this.ag1_email
                    },
                    sender: {
                      firstname: this.firstname,
                      lastname: this.lastname
                    },
                    type: 0
                  });
                }
                if (this.ag2_fname !== '' && this.ag2_lname !== '' && this.status_ag2 === true) {
                  this.aft.collection('messages').add({ //ส่งให้ผู้รับแทนคนที่ 3
                    data: {
                      receiver_parcel: {
                        firstname: this.firstname,
                        lastname: this.lastname
                      },
                      owner: {
                        firstname: this.fname_owner,
                        lastname: this.lname_owner
                      },
                      trackNumber: list.tracking
                    },
                    dateTime: Date(),
                    read: false,
                    receiver: {
                      firstname: this.ag2_fname,
                      lastname: this.ag2_lname,
                      email: this.ag2_email
                    },
                    sender: {
                      firstname: this.firstname,
                      lastname: this.lastname
                    },
                    type: 0
                  });
                }

              }
            })
            this.aft.collection('parcels').doc(parcels.ID).update({
              checkOut: Date(),
              receiver: {
                firstname: this.firstname,
                lastname: this.lastname
              }
            });
            this.snackBar.openFromComponent(PopUpSuccessComponent, {
              horizontalPosition: 'right',
              verticalPosition: 'top',
              duration: 5000,
              panelClass: ['custom-css-class']
            });
            this.modalService.dismissAll();
            setTimeout(() => {
              location.reload();
            }, 3000)
          }
        })
      });
    } else if (checkState === false) {
      alert('พัสดุนี้ไม่อยู่ในรายการของคุณ')
    }
  }
  sendNotifyOwner(rtrack: string) {

  }
}
@Component({
  selector: 'pop-up-success',
  template: `<span >
              <img style="width:3vw" src="../../../assets/images/box.svg" alt="">
              <strong class="ms-5">รับพัสดุเรียบร้อย</strong>
              <button type="button" class="btn float-right" (click)="closeSnackBar()">X</button>
            </span>`,
  styles: [``]
})
export class PopUpSuccessComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}