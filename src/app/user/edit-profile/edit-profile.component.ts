import { Component, EventEmitter, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import firebase from 'firebase/app';
import { UsersModel } from 'src/app/@common/model/users';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  firstname: string = '';
  lastname: string = '';
  department: string = '';
  email: string = '';
  imageProfile: string = '';
  image!: Observable<any>;
  users$: Observable<any[]>;
  emailToken: string = '';
  dataUser: UsersModel[] = [];
  CRUD: any;
  file: any;
  namefile: string = '';
  id: string = '';
  department_En = [
    'เจ้าหน้าที่',
    'วิศวกรรมโยธา', 'วิศวกรรมวัสดุและโลหะการ', 'วิศวกรรมไฟฟ้า',
    'วิศวกรรมเครื่องกล', 'วิศวกรรมอุตสาหการ', 'วิศวกรรมสิ่งทอ',
    'วิศวกรรมอิเล็กทรอนิกส์และโทรคมนาคม', 'วิศวกรรมคอมพิวเตอร์',
    'วิศวกรรมเคมี', 'วิศวกรรมเกษตร'
  ];
  fname: string = '';
  lname: string = '';
  d_partment: string = '';
  e_mail: string = '';
  change_fname: EventEmitter<string> = new EventEmitter();
  change_lname: EventEmitter<string> = new EventEmitter();
  change_department: EventEmitter<string> = new EventEmitter();
  change_email: EventEmitter<string> = new EventEmitter();
  checkKeyCode: EventEmitter<string> = new EventEmitter();
  pass: string = '';
  check_keyCode = '';
  key_code: string | null = null;
  check_Password: string = '';
  checkPass: EventEmitter<string> = new EventEmitter();
  constructor(public modalService: NgbModal, private afstorage: AngularFireStorage, private auth: AuthService, private aft: AngularFirestore, private afAuth: AngularFireAuth) {
    this.users$ = aft.collection('users').valueChanges({ idField: 'ID' });
    this.CRUD = aft.collection('users');
  }

  ngOnInit(): void {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(data => {
      this.emailToken = data.email
    });
    this.users$.subscribe(dataUsers => {
      dataUsers.forEach(data => {
        if (data.role === 'admin') {
          this.key_code = data.key_code;
        }
        if (data.email === this.emailToken) {
          this.dataUser.push(data);
          this.firstname = data.firstname;
          this.lastname = data.lastname;
          this.department = data.department;
          this.email = data.email;
          this.id = data.ID;
          this.imageProfile = data.imageProfile;
          this.pass = data.password;
          if (data.imageProfile !== '') {
            const ref = this.afstorage.ref(this.imageProfile);
            this.image = ref.getDownloadURL();
          }
        }
      })
    });

  }
  resetPassword() {
    const modalRef = this.modalService.open(ResetPasswordComponent);
  }
  upload_Image(e: any) {
    const file = e.target.files[0];
    this.file = file;
    const namefile = e.target.files[0].name;
    this.namefile = namefile;
  }
  changeFirstname(e: any) {
    this.change_fname.emit(e);
    this.fname = e;
  }
  changeLastname(e: any) {
    this.change_lname.emit(e);
    this.lname = e;
  }
  changeDepartment(e: any) {
    this.change_department.emit(e);
    this.d_partment = e;
  }
  changeEmail(e: any) {
    this.change_email.emit(e);
    this.e_mail = e;
  }
  checkKey_code(e: any) {
    this.checkKeyCode.emit(e);
    this.check_keyCode = e;
  }
  update() {
    let n = Date.now();
    let firstname: string = '';
    let lastname: string = '';
    let department: string = '';
    let email: string = '';
    let imageProfile: string = '';
    if (this.namefile !== '') {
      imageProfile = `Profile/${n + this.namefile}`
    } else if (this.namefile === '') {
      imageProfile = this.imageProfile;
    }
    this.fname === '' ? firstname = this.firstname : firstname = this.fname;
    this.lname === '' ? lastname = this.lastname : lastname = this.lname;
    this.d_partment === '' ? department = this.department : department = this.d_partment;
    this.e_mail === '' ? email = this.email : email = this.e_mail;
    if (email === this.email) {
      if (this.check_keyCode !== '') {
        if (confirm('ยืนยันการแก้ไขข้อมูล')) {
          if (this.key_code === this.check_keyCode && this.key_code !== null) {
            this.update_firebase(firstname, lastname, department, imageProfile, email);
          } else {
            alert('รหัสสำหรับแก้ไขข้อมูลไม่ถูกต้อง กรุณาติดต่อเจ้าหน้าที่');
          }
        }
      } else {
        alert('กรุณากรอกรหัสสำหรับแก้ไขข้อมูล หรือติดต่อเจ้าหน้าที่')
      }

    }
  }

  update_firebase(fistname: string, lastname: string, department: string, imageProfile: string, email: string) {
    let n = Date.now();
    if (this.namefile !== '') {
      const task = this.afstorage.upload(imageProfile, this.file);
      task.snapshotChanges()
    }
    this.CRUD.doc(this.id).update({
      firstname: fistname,
      lastname: lastname,
      department: department,
      imageProfile: imageProfile,
      email: email
    }).then((s: any) => { alert('แก้ไขข้อมูลผู้ใช้เรียบร้อย'), location.reload() }).catch((err: any) => console.log(err));
  }
}
@Component({
  selector: 'reset-password',
  template: `<div class="p-3">
    <div class="modal-title">
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="activeModal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-header border-0">

    </div>
    <div class="modal-body">
    <div class="d-flex justify-content-center">
          <strong>แก้ไขรหัสผ่าน</strong>
      </div>
      <div class="row form-group">
        <div>
          <input
            id="password"
            type="password"
            class="mt-4 rounded form-control"
            [(ngModel)]="current_password"
            placeholder="รหัสผ่านปัจจุบัน"
            [class]="required_CurPass"
            required
          />
          <div class="invalid-feedback" *ngIf="check_CurPass === 'not-have'">
            กรุณากรอกรหัสผ่านปัจจุบัน
          </div>
          <div class="invalid-feedback" *ngIf="check_CurPass === 'not-pass'">
            รหัสผ่านไม่ถูกต้อง
          </div>
        </div>
        <div>
          <input
            id="new_password"
            type="password"
            class="mt-3 rounded form-control"
            [(ngModel)]="new_password"
            placeholder="รหัสผ่านใหม่"
            [class]="required_newPass"
            required
          />
          <div class="invalid-feedback" *ngIf="check_newPass === 'not-have'">
            กรุณากรอกรหัสผ่านใหม่
          </div>
          <div class="invalid-feedback" *ngIf="check_newPass ==='require-6-char'">
            รหัสผ่านต้องไม่ต่ำกว่า 6 ตัว
          </div>
        </div>
        <div>
        <input
          id="check_password"
          type="password"
          class="mt-2 rounded form-control"
          [(ngModel)]="verify_password"
          placeholder="ยืนยันรหัสผ่านใหม่"
          [class]="required_verify"
          required
        />
        <div class="invalid-feedback" *ngIf="check_verifyPass ==='pass'">
            กรุณายืนยันรหัสผ่านใหม่
          </div>
          <div class="invalid-feedback" *ngIf="check_verifyPass ==='not-pass'">
            รหัสผ่านไม่ตรงกัน
          </div>
        </div>
      </div>
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck1" (change)="show_password($event)">
        <label class="custom-control-label" for="customCheck1">แสดงรหัสผ่าน</label>
      </div>


      <div class="row mt-3">
        <button
          class="btnedit"
          mat-flat-button
          (click)="resetPassword()"
        >
          ยืนยัน
        </button>
      </div>
    </div>
  </div>`,
  styles: [
    `
    .btnedit{
        background-color: #30009C;
        color: white;
    }
}`,
  ],
})
export class ResetPasswordComponent {
  password = '';
  current_password = '';
  new_password = '';
  verify_password = '';
  user$: Observable<any[]>;
  id = "";
  required_CurPass = '';
  required_newPass = '';
  required_verify = '';
  check_newPass = '';
  check_CurPass = '';
  check_verifyPass = '';
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.user$ = aft.collection('users').valueChanges({ idField: 'ID' });
  }
  ngOnInit(): void {
    const email_user = firebase.auth().currentUser?.email;
    this.user$.subscribe(data => {
      data.forEach((user) => {
        if (user.email === email_user) {
          this.id = user.ID;
          this.password = atob(`${user.password}`);
        }
      })
    })
  }
  show_password(event: any) {
    let input_password: any = document.getElementById('password');
    let new_password: any = document.getElementById('new_password');
    let check_password: any = document.getElementById('check_password');
    try {
      if (event.target.checked === true) {
        input_password.type = 'text';
        new_password.type = 'text';
        check_password.type = 'text';
      } else {
        input_password.type = 'password';
        new_password.type = 'password';
        check_password.type = 'password';
      }
    } catch {

    }
  }
  resetPassword() {
    if (this.current_password !== '') {
      if (this.current_password === this.password) { //ตรวจสอบรหัสผ่านเดิม
        this.required_CurPass = '';
        if (this.new_password !== '') { //รหัสผ่านใหม่
          if (this.new_password.length >= 6) { //รหัสผ่านใหม่ต้องไม่น้อยกว่า 6 ตัว
            this.required_newPass = '';
            if (this.verify_password !== '') {
              if (this.verify_password === this.new_password) { //ยืนยันรหัสผ่านใหม่
                this.required_verify = '';
                this.check_verifyPass = '';
                this.update();
              } else {
                this.required_verify = 'is-invalid';
                this.check_verifyPass = 'not-pass'
              }
            } else { // ไม่ยืนยันรหัส
              this.required_verify = 'is-invalid';
              this.check_verifyPass = 'pass'
            }
          } else {
            this.required_newPass = 'is-invalid';
            this.check_newPass = 'require-6-char'
          }
        } else { //รหัสผ่านใหม่ไม่มี
          this.required_newPass = 'is-invalid';
          this.check_newPass = 'not-have';
        }
      } else {
        this.required_CurPass = 'is-invalid'
        this.check_CurPass = 'not-pass'
      }
    } else {
      this.required_CurPass = 'is-invalid';
      this.check_CurPass = 'not-have';
    }
  }
  update() {
    const user = firebase.auth().currentUser;
    const encode = btoa(`${this.new_password}`);
    user?.updatePassword(this.new_password).then(() => {
      this.aft.collection('users').doc(this.id).update({
        password: encode
      }).then(() => {
        this.snackBar.openFromComponent(PopupResetPasswordComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        })
        this.activeModal.dismiss();
      }).catch((err) => console.log(err))
    }).catch((err) => {
      alert('แก้ไขรหัสผ่านไม่สำเร็จ กรูณาตรวจสอบหรือทำการล็อกอินใหม่');
    })
  }
}
@Component({
  selector: 'remove-parcel',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-success">check_circle</mat-icon>
    <strong class="ms-5">แก้ไขรหัสผ่านเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopupResetPasswordComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
