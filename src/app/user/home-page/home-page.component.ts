import { AfterViewInit, Component, EventEmitter, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import jwtDecode from 'jwt-decode';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit, AfterViewInit {
  listWaitParcels$: Observable<any[]>;
  statusPage: number = 0;
  statusParcels: boolean = false;
  list_Parcel0: any[] = [];
  list_Parcel1: any[] = [];
  name = '';
  receiverName = '';
  track = '';
  date = '';
  name_Filter: string = '';
  receiver_Filter: string = '';
  track_Filter: string = '';
  date_Filter: string = '';
  owner: object[] = [];
  receiver: any[] = [];
  trackNumber: any[] = [];
  timeStamp: string[] = [];
  list_tracking: any[] = []
  setDate = '';
  deCodeName: string = '';
  users$: Observable<any[]>;
  search_Name: EventEmitter<string> = new EventEmitter();
  search_Track: EventEmitter<string> = new EventEmitter();
  search_Date: EventEmitter<string> = new EventEmitter();
  tracking: string = '';
  CRUD: any;
  stateSearch: number = 1;
  selectSearch: string = 'หมายเลขติดตามพัสดุ';
  model = '';
  assignor: any[] = [];
  email = '';
  data_user: any = {};
  status_submit_parcel: boolean = false;
  length_track: number = 0
  get_name: string = '';
  constructor(public modalService: NgbModal, private firestore: AngularFirestore, private cookieService: CookieService, private snackBar: MatSnackBar) {
    this.listWaitParcels$ = firestore.collection('parcels', ref => ref.orderBy('checkIn', 'desc')).valueChanges({ idField: 'ID' });
    this.users$ = firestore.collection('users').valueChanges();
    this.CRUD = firestore.collection('parcels');
  }
  ngOnInit(): void {
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach((t_email) => {
      this.users$.subscribe((data) => {
        data.forEach((user) => {
          if (user.email === t_email.email) {
            this.deCodeName = user.firstname + user.lastname;
            this.data_user = {
              firstname: user.firstname,
              lastname: user.lastname,
              department: user.department
            };
          }
        })
        data.forEach(data => {
          if (data.agent.agent0.firstname + data.agent.agent0.lastname === this.deCodeName) {
            this.assignor.push(data.firstname + data.lastname);
          } else if (data.agent.agent1.firstname + data.agent.agent1.lastname === this.deCodeName) {
            this.assignor.push(data.firstname + data.lastname);
          } else if (data.agent.agent2.firstname + data.agent.agent2.lastname === this.deCodeName) {
            this.assignor.push(data.firstname + data.lastname);
          }
        })
      })
    })

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.search();
    }, 300)
  }
  add_tracking() {
    if (this.tracking !== '') {
      const setArr = this.list_tracking.find((data) => data === this.tracking);
      const find = this.trackNumber.find(d => d === this.tracking);
      if (find === undefined) {
        if (setArr === undefined) {
          this.list_tracking.push(this.tracking.toUpperCase());
          this.length_track++;
          this.tracking = '';
        }
      } else {
        alert('หมายเลข' + ' ' + this.tracking + ' ' + 'มีการลงทะเบียนไว้แล้ว')
      }
    }
  }
  delete_track(track: string) {
    function arrayRemove(arr: any, value: any) {
      return arr.filter(function (ele: any) {
        return ele != value;
      })
    }
    const list_track = this.list_tracking.find(data => data === track)
    const result = arrayRemove(this.list_tracking, list_track);
    this.list_tracking = result;
    this.length_track--;
  }

  addParcel(content: any): void {
    this.modalService.open(content);
  }
  changeState_search(state: number, select: string) {
    this.stateSearch = state
    this.selectSearch = select;
  }
  resetForm() {
    this.searchName('');
    this.searchTrack('');
    this.searchDate('');
  }
  filterItems(arr: any[], query: string) {
    return arr.filter(function (el) {
      return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
  }
  searchName(name: string) {
    this.search_Name.emit(name);
    this.name = name;
    this.search();
  }
  searchReceiver(receiver: string) {
    this.search_Name.emit(receiver);
    this.receiverName = receiver;
    this.search();
  }
  searchTrack(track: string) {
    this.search_Name.emit(track);
    this.track = track;
    this.search();
  }
  searchDate(date: string) {
    this.search_Name.emit(date);
    let dateTime = new Date(date),
      month = '' + (dateTime.getMonth() + 1),
      day = '' + dateTime.getDate(),
      year = '' + dateTime.getFullYear();
    this.date = date;
    this.search();
    return [year, month, day].join('-');
  }
  search() {
    console.log(this.assignor)
    this.listWaitParcels$.subscribe((data) => {
      return (this.list_Parcel0 = []), (this.list_Parcel1 = []);
    });
    this.listWaitParcels$.subscribe((dataFromFireStore) => {
      dataFromFireStore.forEach((item) => {
        const data_name = this.deCodeName
        const item_owner = item.owner.firstname + item.owner.lastname;
        const item_receiver = item.receiver.firstname + item.receiver.lastname;
        this.receiver.push(item_receiver);
        this.owner.push(item_owner);
        this.trackNumber.push(item.trackNumber);

        if (item_owner === data_name) {
          if (item.checkOut === null) {
            const setDate = (itemD: any) => {
              let dateTime = new Date(itemD),
                month = '' + (dateTime.getMonth() + 1),
                day = '' + dateTime.getDate(),
                year = '' + dateTime.getFullYear();
              if (month.length < 2) {
                month = '0' + month;
              }
              if (day.length < 2) {
                day = '0' + day;
              }
              return [year, month, day].join('-');
            };
            const setD = setDate(item.checkIn);
            this.timeStamp.push(setD);
            const filter_name = this.filterItems(this.owner, this.name);
            filter_name.forEach((filter) => {
              let name = '';
              for (let i of filter) {
                name += i;
              }
              this.name_Filter = name;
            });
            const filter_track = this.filterItems(this.trackNumber, this.track);
            filter_track.forEach((filter) => {
              let track = '';
              for (let i of filter) {
                track += i;
              }
              this.track_Filter = track;
            });
            const filter_date = this.filterItems(this.timeStamp, this.date);
            filter_date.forEach((filter) => {
              let date = '';
              for (let i of filter) {
                date += i;
              }
              this.date_Filter = date;
            });
            if (
              item_owner === this.name_Filter &&
              item_receiver === this.receiver_Filter &&
              item.trackNumber === this.track_Filter &&
              setD === this.date_Filter
            ) {
              this.list_Parcel0.push(item);
              this.statusParcels = true;
            }
          } else {
            this.list_Parcel1.push(item);
          }
        }
      });
    });
  }
  activeStatus(p: number) {
    this.statusPage = p;
  }
  addTracking() {
    if (this.length_track > 0) {
      this.list_tracking.forEach((track) => {
        this.crud_Firestore(this.data_user.firstname, this.data_user.lastname, this.data_user.department, track)
      })
    } else if (this.length_track === 0) {
      alert('กรุณาเพิ่มหมายเลขติดตามพัสดุ');
    }
  }
  crud_Firestore(firstname: string, lastname: string, department: string, tracking: string) {
    try {
      this.CRUD.add({
        owner: {
          firstname: firstname,
          lastname: lastname
        },
        receiver: {
          firstname: '',
          lastname: ''
        },
        trackNumber: tracking,
        department: department,
        checkIn: null,
        checkOut: null,
        images: {
          image0: '',
          image1: '',
          image2: '',
          image3: ''
        }
      })
    } catch (err) {
      alert('บันทึกหมายเลขพัสดุไม่สำเร็จ')
    } finally {
      this.snackBar.openFromComponent(PopUpAddTrackingSuccessComponent, {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 5000,
        panelClass: ['custom-css-class']
      });
      this.tracking = '';
      this.length_track = 0;
      this.list_tracking = [];
    }

  }
}
@Component({
  selector: 'add-tracking',
  template: `<span >
                <img style="width:3vw" src="../../../assets/images/box.svg" alt="">
                <strong class="ms-5">บันทึกพัสดุสำเร็จ</strong>
                <button type="button" class="btn float-right" (click)="closeSnackBar()">X</button>
              </span>`,
  styles: [``]
})
export class PopUpAddTrackingSuccessComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
