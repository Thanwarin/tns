import { Component, Input, OnInit } from '@angular/core';
import { ParcelsModel } from '../../../@common/model/parcels';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from '@angular/fire/storage/';

import { Observable } from 'rxjs';
import { StatusParcelsModel } from 'src/app/@common/enums/statusParcels.enum';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import jwtDecode from 'jwt-decode';
import { AgentModel } from 'src/app/@common/model/users';

@Component({
  selector: 'app-card-parcels',
  templateUrl: './card-parcels.component.html',
  styleUrls: ['./card-parcels.component.scss'],
})
export class CardParcelsComponent implements OnInit {
  @Input() listParcels: ParcelsModel = {};
  @Input() statusParcels: StatusParcelsModel = 0;
  @Input() owner: string = '';
  ownerName: string = '';
  editBtn: boolean = false;
  nDate = new Date();
  date = '';
  time = '';
  data$: Observable<any[]>;
  agent: AgentModel = {};
  agent0: object = {};
  agent1: object = {};
  agent2: object = {};
  email: string = '';
  listImage!: {
    Img0: Observable<any>;
    Img1: Observable<any>;
    Img2: Observable<any>;
    Img3: Observable<any>;
  };
  name: string = ''
  storage!: Observable<string | null>;
  constructor(
    public modalService: NgbModal,
    private angularstorage: AngularFireStorage,
    public config: NgbCarouselConfig,
    private aft: AngularFirestore
  ) {
    this.data$ = aft.collection('users').valueChanges({ idField: 'ID' });
  }
  ngOnInit(): void {
    const remove = (arr: any) => [...new Set(arr)];
    const data: any = sessionStorage.getItem('token');
    const token: any[] = [];
    token.push(jwtDecode(data));
    token.forEach(tk_email => {
      this.email = tk_email.email;
    });
    this.data$.subscribe(data => {
      data.forEach(user => {
        if (user.email === this.email) {
          this.name = user.firstname + ' ' + user.lastname;
        }
      })
    })
    this.ownerName =
      `${this.listParcels.owner?.firstname}` +
      `${this.listParcels.owner?.lastname}`;
    const Img0 = this.angularstorage
      .ref(`${this.listParcels.images?.image0}`)
      .getDownloadURL();
    const Img1 = this.angularstorage
      .ref(`${this.listParcels.images?.image1}`)
      .getDownloadURL();
    const Img2 = this.angularstorage
      .ref(`${this.listParcels.images?.image2}`)
      .getDownloadURL();
    const Img3 = this.angularstorage
      .ref(`${this.listParcels.images?.image3}`)
      .getDownloadURL();
    const listImg = { Img0, Img1, Img2, Img3 };
    this.listImage = listImg;
    const ndate = new Date(`${this.listParcels.checkIn}`);
    const resultDate = ndate.toLocaleDateString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
    const resultTime = ndate.toLocaleTimeString('th-TH', {
      hour: '2-digit',
      minute: 'numeric'
    });
    this.date = resultDate;
    this.time = resultTime;
  }
  showImage(content: any): void {
    this.modalService.open(content);
  }
  editParcel() {
    const modalRef = this.modalService.open(EditParcelComponent);
    const sendValue = modalRef.componentInstance;
    sendValue.data = this.listParcels;
  }
}
@Component({
  selector: 'assign-receive',
  template: `<div class="p-3">
  <div class="modal-title">
    <button
      type="button"
      class="close"
      aria-label="Close"
      (click)="activeModal.dismiss('Cross click')"
    >
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-header border-0">
  <div class="row text-left">
      <div class="col">
        <img src="../../../assets/images/box.svg" class="w-50">
      </div>
      <div class="col">
        <div class="row">
        <p>หมายเลขติดตามพัสดุ</p>
        </div>
        <div class="row">
          <p>{{track}}</p>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-body">
    <div class="row">
      <p class="text-secondary">วันที่เช็คอิน</p>
      <p *ngIf="dateTime !== 'Invalid Date'">{{dateTime}} น.</p>
      <p *ngIf="dateTime === 'Invalid Date'">-</p>
    </div>
    <div class="custom-control custom-radio custom-control-inline">
      <input [checked]="check0" (change)="check_StatusParcel('รับเอง')" type="radio" id="customRadioInline1" name="customRadioInline" class="custom-control-input">
      <label class="custom-control-label" for="customRadioInline1">รับเอง</label>
    </div>
    <div class="custom-control custom-radio custom-control-inline">
      <input [checked]="check1" (change)="check_StatusParcel('รับแทน')" type="radio" id="customRadioInline2" name="customRadioInline" class="custom-control-input">
      <label class="custom-control-label" for="customRadioInline2">รับแทน</label>
    </div>
    <div *ngIf="statusParcel === 'รับแทน'">
      <p>ผู้รับพัสดุแทนของคุณ : {{fn_receiver}} {{ln_receiver}}</p> 
      <div class="d-flex flex-row">
      <div class="d-flex justify-content-center px-2" *ngFor="let ag of agents">
        <button mat-mini-fab class="bg-cl" (click)="toggleBadgeVisibility(ag.fln)">
          <strong class="text-white">{{ag.fln}}</strong>
          <span class="badge badge-danger position-absolute mt-3" *ngIf="ag.check === true">เลือก</span>
        </button>
      </div>
    </div>
    </div>
    <div class="row mt-3">
      <button
        class="btn"
        mat-flat-button
        (click)="activeModal.dismiss('Cross click')"
        (click)="update_receiver()"
      >
        บันทึก
      </button>
    </div>
  </div>
</div>`,
  styles: [`
  .btn{
      background-color: #30009C;
      color: white;
  }
  .bg-cl{
    background-color: #30009C;
  }
}`],
})
export class AssignReceiveParcelComponent {
  @Input() protected data: ParcelsModel = {};
  @Input() public dateTime: string = '';
  @Input() public agent: AgentModel = {};
  statusParcel: string = '';
  track: string = '';
  CRUD: any;
  id: any[] = [];
  name_ag0: string = '';
  name_ag1: string = '';
  name_ag2: string = '';
  check0: string = '';
  check1: string = '';
  hidden: boolean = false;
  agents: any[] = [];
  fn_receiver: string = '';
  ln_receiver: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.CRUD = aft.collection('parcels');

  }
  ngOnInit(): void {
    this.track = `${this.data.trackNumber}`;
    this.id.push(this.data);
    this.id.forEach((data) => {
      return (this.id = data.ID);
    });
    this.fn_receiver = `${this.data.receiver?.firstname}`;
    this.ln_receiver = `${this.data.receiver?.lastname}`;
    const ag0 = this.agent.agent0;
    const ag1 = this.agent.agent1;
    const ag2 = this.agent.agent2;
    const receiver = this.data.receiver;
    const rv_fln = `${receiver?.firstname}`.substring(1, -1).toUpperCase() + `${receiver?.lastname}`.substring(1, -1).toUpperCase();
    if (
      ag0?.status_receiver === true) {
      const fn0 = `${ag0?.firstname}`.substring(1, -1).toUpperCase();
      const ln0 = `${ag0?.lastname}`.substring(1, -1).toUpperCase();
      const fln0 = fn0 + ln0;
      this.name_ag0 = fln0;
      const agent0 = {
        fln: fln0,
        check: false,
      }
      if (rv_fln === '') {
        this.agents.push(agent0);
      } else if (rv_fln !== '') {
        if (rv_fln === fln0) {
          agent0.check = true;
        } else {
          agent0.check = false;
        }
        this.agents.push(agent0)
      }
    }

    if (ag1?.status_receiver === true) {
      const fn1 = `${ag1?.firstname}`.substring(1, -1).toUpperCase();
      const ln1 = `${ag1?.lastname}`.substring(1, -1).toUpperCase();
      const fln1 = fn1 + ln1;
      this.name_ag1 = fln1;
      const agent1 = {
        fln: fln1,
        check: false,
      }
      if (rv_fln === '') {
        this.agents.push(agent1);
      } else if (rv_fln !== '') {
        if (rv_fln === fln1) {
          agent1.check = true;
        } else {
          agent1.check = false;
        }
        this.agents.push(agent1)
      }
    }
    if (ag2?.status_receiver === true) {
      const fn2 = `${ag2?.firstname}`.substring(1, -1).toUpperCase();
      const ln2 = `${ag2?.lastname}`.substring(1, -1).toUpperCase();
      const fln2 = fn2 + ln2;
      this.name_ag2 = fln2;
      const agent2 = {
        fln: fln2,
        check: false,
      }
      if (rv_fln === '') {
        this.agents.push(agent2);
      } else if (rv_fln !== '') {
        if (rv_fln === fln2) {
          agent2.check = true;
        } else {
          agent2.check = false;
        }
        this.agents.push(agent2)
      }
    }
    const name = `${this.data.receiver?.firstname}` + `${this.data.receiver?.lastname}`;
    if (name === '') {
      this.check0 = 'checked';
      this.check1 = '';
      this.statusParcel = 'รับเอง';
    } else {
      this.check0 = '';
      this.check1 = 'checked'
      this.statusParcel = 'รับแทน';
    }
  }
  check_StatusParcel(status: string) {
    if (status === 'รับเอง') {
      this.statusParcel = status;
      this.check0 = 'checked';
      this.fn_receiver = '';
      this.ln_receiver = '';
    } else if (status === 'รับแทน') {
      this.statusParcel = status
      this.check1 = 'checked';
    }
  }
  toggleBadgeVisibility(name: string) {
    const count = this.agents.length;
    switch (count) {
      case 1:
        if (name === this.name_ag0) {
          this.agents[0].check = true;
        }
        break;
      case 2:
        if (name === this.name_ag0) {
          this.agents[0].check = true;
          this.agents[1].check = false;
        } else if (name = this.name_ag1) {
          this.agents[0].check = false;
          this.agents[1].check = true;
        }
        break;
      case 3:
        if (name === this.name_ag0) {
          this.agents[0].check = true;
          this.agents[1].check = false;
          this.agents[2].check = false;
        } else if (name = this.name_ag1) {
          this.agents[0].check = false;
          this.agents[1].check = true;
          this.agents[2].check = false;
        }
        else if (name = this.name_ag2) {
          this.agents[0].check = false;
          this.agents[1].check = false;
          this.agents[2].check = true;
        }
        break;
    }
    const result = this.agents.filter(ag => ag.check == true);
    const ag0 = `${this.agent.agent0?.firstname}`.substring(1, -1).toUpperCase() + `${this.agent.agent0?.lastname}`.substring(1, -1).toUpperCase();
    const ag1 = `${this.agent.agent1?.firstname}`.substring(1, -1).toUpperCase() + `${this.agent.agent1?.lastname}`.substring(1, -1).toUpperCase();
    const ag2 = `${this.agent.agent2?.firstname}`.substring(1, -1).toUpperCase() + `${this.agent.agent2?.lastname}`.substring(1, -1).toUpperCase();
    result.forEach(data => {
      if (data.fln === ag0) {
        this.fn_receiver = `${this.agent.agent0?.firstname}`;
        this.ln_receiver = `${this.agent.agent0?.lastname}`;
      } else if (data.fln === ag1) {
        this.fn_receiver = `${this.agent.agent1?.firstname}`;
        this.ln_receiver = `${this.agent.agent1?.lastname}`;
      } else if (data.fln === ag2) {
        this.fn_receiver = `${this.agent.agent2?.firstname}`;
        this.ln_receiver = `${this.agent.agent2?.lastname}`;
      }
    })
  }
  update_receiver() {
    this.CRUD.doc(this.id).update({
      owner: {
        firstname: this.data.owner?.firstname,
        lastname: this.data.owner?.lastname
      },
      receiver: {
        firstname: this.fn_receiver,
        lastname: this.ln_receiver
      },
      trackNumber: this.data.trackNumber,
      department: this.data.department,
      checkIn: this.data.checkIn,
      checkOut: this.data.checkOut,
      images: {
        image0: this.data.images?.image0,
        image1: this.data.images?.image1,
        image2: this.data.images?.image2,
        image3: this.data.images?.image3,
      },
    })
  }
}
@Component({
  selector: 'edit-parcel',
  template: `<div class="p-3">
    <div class="modal-title">
      <button mat-stroked-button color="warn" class="border-danger rounded-pill" (click)="removeParcel()">
        ลบรายการพัสดุ
        <mat-icon>delete</mat-icon>
      </button>
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="activeModal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-header border-0">
      <div class="row">
        <div class="d-flex justify-content-center">
          <img src="../../../assets/images/box.svg" class="w-50" alt="" />
        </div>
        <div class="d-flex justify-content-center mt-3">
          <strong>แก้ไขรายการพัสดุของคุณ</strong>
        </div>
      </div>
    </div>
    <div class="modal-body">
      <div class="row">
        <p>หมายเลขติดตามพัสดุ</p>
      </div>
      <div class="row">
        <input
          type="text"
          class="text-center"
          [(ngModel)]="track"
          placeholder="กรอกหมายเลขติดตามพัสดุ"
        />
      </div>
      <div class="row mt-3">
        <button
          class="btnedit"
          mat-flat-button
          (click)="editTracking()"
          (click)="activeModal.dismiss('Cross click')"
        >
          บันทึก
        </button>
      </div>
    </div>
  </div>`,
  styles: [
    `
    .btnedit{
        background-color: #30009C;
        color: white;
    }
}`,
  ],
})
export class EditParcelComponent {
  @Input() protected data: ParcelsModel = {};
  track: string = '';
  CRUD: any;
  id: any[] = [];
  constructor(
    public activeModal: NgbActiveModal,
    private aft: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.CRUD = aft.collection('parcels');
  }
  ngOnInit(): void {
    this.track = `${this.data.trackNumber}`;
    this.id.push(this.data);
    this.id.forEach((data) => {
      return (this.id = data.ID);
    });
  }

  removeParcel() {
    if (confirm('ยืนยันลบรายการพัสดุหมายเลข : ' + this.track)) {
      try {
        this.CRUD.doc(this.id).delete();
      } catch {
        alert('ลบรายการพัสดุไม่สำเร็จ')
      } finally {
        this.snackBar.openFromComponent(PopUpRemoveParcelComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
        this.activeModal.dismiss();
      }
    }
  }
  editTracking() {
    try {
      this.CRUD.doc(this.id).update({
        owner: {
          firstname: this.data.owner?.firstname,
          lastname: this.data.owner?.lastname,
        },
        receiver: {
          firstname: this.data.receiver?.firstname,
          lastname: this.data.receiver?.lastname,
        },
        trackNumber: this.track,
        department: this.data.department,
        checkIn: this.data.checkIn,
        checkOut: this.data.checkOut,
        images: {
          image0: this.data.images?.image0,
          image1: this.data.images?.image1,
          image2: this.data.images?.image2,
          image3: this.data.images?.image3,
        },
      });
    } catch (err) {
      alert('แก้ไขหมายเลขติดตามพัสดุไม่สำเร็จ');
    } finally {
      this.snackBar.openFromComponent(PopUpAddTrackingSuccessComponent, {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 5000,
        panelClass: ['custom-css-class'],
      });
    }
  }
}
@Component({
  selector: 'add-tracking',
  template: `<span>
    <img style="width:3vw" src="../../../assets/images/box.svg" alt="" />
    <strong class="ms-5">แก้ไขรายการพัสดุเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpAddTrackingSuccessComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}

@Component({
  selector: 'remove-parcel',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-danger">delete</mat-icon>
    <strong class="ms-5">ลบรายการพัสดุเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopUpRemoveParcelComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
