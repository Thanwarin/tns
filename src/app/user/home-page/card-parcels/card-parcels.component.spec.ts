import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardParcelsComponent } from './card-parcels.component';

describe('CardParcelsComponent', () => {
  let component: CardParcelsComponent;
  let fixture: ComponentFixture<CardParcelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardParcelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardParcelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
