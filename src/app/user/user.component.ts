import { Component, EventEmitter, HostListener, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  statusPage: string = 'home-page';
  page: EventEmitter<string> = new EventEmitter;
  constructor(private Auth: AngularFireAuth, private auth: AuthService, private router: Router) {

  }
  ngOnInit(): void {
    const page = sessionStorage.getItem('page');
    this.statusPage = `${page}`;
    const authToken = this.Auth.idToken.subscribe(data => {
      if (data === null) {
        this.auth.logout();
      }
    });
    const role = sessionStorage.getItem('role');
    if (role === 'admin') {
      this.router.navigateByUrl('/admin');
    }
  }
  goTo(p: any) {
    this.page.emit(p);
    const page = sessionStorage.getItem('page');
    this.statusPage = `${page}`;
  }
}