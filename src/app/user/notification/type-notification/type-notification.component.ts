import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { UsersModel } from 'src/app/@common/model/users';

@Component({
  selector: 'app-type-notification',
  templateUrl: './type-notification.component.html',
  styleUrls: ['./type-notification.component.scss']
})
export class TypeNotificationComponent implements OnInit {
  @Input() messages: any = {};
  @Input() firstname: string = '';
  @Input() lastname: string = '';
  @Input() email: string = '';
  id_message: string = '';
  dateTime = '';
  sender_fname: string = '';
  sender_lname: string = '';
  path_Profile: string = '';
  image_Profile!: Observable<any>;
  data_owner: UsersModel = {};
  id_owner: string = '';
  constructor(private angularstorage: AngularFireStorage, private aft: AngularFirestore, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    const dateTime = new Date(`${this.messages.dateTime}`);
    const result_dateTime = dateTime.toLocaleTimeString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: '2-digit',
      minute: 'numeric',
    });
    this.id_message = this.messages.ID;
    this.dateTime = result_dateTime;
    const dataUser = this.aft.collection('users').valueChanges({ idField: 'ID' }).subscribe((data => {
      data.forEach((users: any) => {
        if (users.firstname === this.messages.sender?.firstname && users.lastname === this.messages.sender?.lastname) {
          this.path_Profile = users.imageProfile;
          this.data_owner = users;
          this.id_owner = users.ID;
          if (users.imageProfile !== '') {
            const image = this.angularstorage.ref(`${users.imageProfile}`).getDownloadURL();
            this.image_Profile = image;
          }
          const fname = `${users.firstname}`;
          const lname = `${users.lastname}`;
          this.sender_fname = fname.toUpperCase().substr(0, 1);
          this.sender_lname = lname.toUpperCase().substr(0, 1);
        }
      })
    }));
  }
  accept() {
    if (this.data_owner.agent?.agent0?.firstname === '' && this.data_owner.agent.agent0.lastname === '') {
      this.aft.collection('users').doc(this.id_owner).update({
        agent: {
          agent0: {
            firstname: this.firstname,
            lastname: this.lastname,
            status_receiver: true,
            email: this.email
          },
          agent1: this.data_owner.agent.agent1,
          agent2: this.data_owner.agent.agent2
        }
      }).then(() => {
        this.aft.collection('messages').doc(this.id_message).delete().then(() => {
          setTimeout(() => {
            window.location.reload();
          }, 300)

        });;
        this.snackBar.openFromComponent(PopupNotifyAgentComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
      }).catch((err) => {
        alert('ดำเนินการยอมรับไม่สำเร็จ')
      })
    } else if (this.data_owner.agent?.agent1?.firstname === '' && this.data_owner.agent.agent1.lastname === '') {
      this.aft.collection('users').doc(this.id_owner).update({
        agent: {
          agent0: this.data_owner.agent.agent0,
          agent1: {
            firstname: this.firstname,
            lastname: this.lastname,
            status_receiver: true,
            email: this.email
          },
          agent2: this.data_owner.agent.agent2
        }
      }).then(() => {
        this.aft.collection('messages').doc(this.id_message).delete().then(() => {
          setTimeout(() => {
            window.location.reload();
          }, 300)

        });
        this.snackBar.openFromComponent(PopupNotifyAgentComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
      }).catch((err) => {
        alert('ดำเนินการยอมรับไม่สำเร็จ')
      })
    } else if (this.data_owner.agent?.agent2?.firstname === '' && this.data_owner.agent.agent2.lastname === '') {
      this.aft.collection('users').doc(this.id_owner).update({
        agent: {
          agent0: this.data_owner.agent.agent0,
          agent1: this.data_owner.agent.agent1,
          agent2: {
            firstname: this.firstname,
            lastname: this.lastname,
            status_receiver: true,
            email: this.email
          }
        }
      }).then(() => {
        this.aft.collection('messages').doc(this.id_message).delete().then(() => {
          setTimeout(() => {
            window.location.reload();
          }, 300)
        });
        this.snackBar.openFromComponent(PopupNotifyAgentComponent, {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
          panelClass: ['custom-css-class'],
        });
      }).catch((err) => {
        alert('ดำเนินการยอมรับไม่สำเร็จ')
      })
    } else {
      alert('ไม่มีคำขอนี้แล้ว หรือ ผู้ใช้เพิ่มตัวแทนรับพัสดุครบจำนวนแล้ว');
    }

  }
  cancel() {
    this.aft.collection('messages').doc(this.id_message).delete().then(() => {
      window.location.reload();
    });
  }

}

@Component({
  selector: 'popup-notify-agent',
  template: `<span class="d-flex align-items-center">
    <mat-icon class="text-success">check_circle</mat-icon>
    <strong class="ms-5">ดำเนินการเรียบร้อย</strong>
    <button type="button" class="btn float-right" (click)="closeSnackBar()">
      X
    </button>
  </span>`,
  styles: [``],
})
export class PopupNotifyAgentComponent {
  constructor(private snackBar: MatSnackBar) { }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
