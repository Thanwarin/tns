import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeNotificationComponent } from './type-notification.component';

describe('TypeNotificationComponent', () => {
  let component: TypeNotificationComponent;
  let fixture: ComponentFixture<TypeNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeNotificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
