import { AngularFireAuthGuard, hasCustomClaim } from '@angular/fire/auth-guard';
import { Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';

const adminOnly = () => hasCustomClaim('admin');

export const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'welcome', component: HomeComponent },
  {
    path: 'user', component: UserComponent,
    canActivate: [AngularFireAuthGuard]
  },
  {
    path: 'admin', component: AdminComponent,
    canActivate: [AngularFireAuthGuard]
  }
];