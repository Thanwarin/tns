export class DateModel {
    date?: string;
    year?: string;
    month?: string;

}
export class YearsModel{
        y?:string;
        month?:[MonthModel];
}

export class MonthModel{
        Jan?:string[];
        Feb?:string[];
        Mar?:string[];
        Apr?:string[];
        May?:string[];
        Jun?:string[];
        Jul?:string[];
        Aug?:string[];
        Sep?:string[];
        Oct?:string[];
        Nov?:string[];
        Dec?:string[]
}