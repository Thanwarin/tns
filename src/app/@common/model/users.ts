export class UsersModel{
    firstname?: string;
    lastname?: string;
    email?: string;
    department?: string;
    imageProfile?: string;
    agent?:AgentModel
    password?: string;
    role?: string
}
export class AgentModel{
    agent0?: {
        firstname?: string;
        lastname?: string;
        status_receiver?: boolean;
        email?:string;
    };
    agent1?: {
        firstname?: string;
        lastname?: string;
        status_receiver?: boolean;
        email?:string;
    };
    agent2?: {
        firstname?: string;
        lastname?: string;
        status_receiver?: boolean;
        email?:string;
    };
}
/*
    firstname: ธันวริณท์
    lastname: เที่ยงตรง
    email: i.pech.dirgee@gmail.com
    department: วิศวกรรมคอมพิวเตอร์
    phoneNumber: 0970055407
    imageProfile: Profile/48062921_2100663353346022_5558898999240425472_n.jpg
    agent:{
        agent0: {
            firstname: ธันวริณท์
            lastname: เที่ยงตรง
        };
        agent1: {
            firstname: ธันวริณท์
            lastname: เที่ยงตรง
        };
        agent2: {
            firstname: ธันวริณท์
            lastname: เที่ยงตรง
        };
    };
    password: XFQffwecq554376
*/