// export class MessagesModel{
//     message0?:[MsgModel];
//     message1?:[MsgModel];
//     message2?:[MsgModel];
//     message3?:[MsgModel];
//     message4?:[MsgModel];
//     receiver?: {
//         firstname?: string;
//         lastname?: string;
//     }
//     sender?:{
//         firstname?: string;
//         lastname?: string;
//     }
// }
// export class MsgModel{
//     data?: string;
//     dateTime?: string;
// }
export class MessagesModel{
    data?:{
        trackNumber?:string;
        receiver_parcel?:{
            firstname?:string;
            lastname?:string;
        };
        owner?:{
            firstname?:string;
            lastnamae?:string;
        }
    };
    dateTime?:Date;
    read?:boolean;
    receiver?:{
        firstname?:string;
        lastname?:string;
        email?:string;
    };
    sender?:{
        firstname?:string;
        lastname?:string;
    };
    type?:number;
}