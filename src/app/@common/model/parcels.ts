export class ParcelsModel {
    owner?: {
        firstname?: string;
        lastname?: string;
    }
    receiver?: {
        firstname?: string;
        lastname?: string;
    }
    trackNumber?: string;
    department?: string;
    checkIn?: Date;
    checkOut?: Date;
    images?: {
        image0?: string;
        image1?: string;
        image2?: string;
        image3?: string;
    };
}

/* 
owner = {
    firstname: ธันวริณท์,
    lastname: เที่ยงตรง
}
receiver = {
    firestname: ธนดล,   เป็นเจ้าของหรือผู้รับแทนก็ได้
    lastname: กลิ่นสุคนธ์
}
trackNumber: RH145478421TH
department: วิศวกรรมคอมพิวเตอร์
checkIn: Mon May 24 2021 20:56:16 GMT+0700 (เวลาอินโดจีน)
checkOut: Mon May 24 2021 20:56:16 GMT+0700 (เวลาอินโดจีน) ถ้ายังไม่ได้เช็ค type จะเป็น null
images = {
    image0:Parcels/mountains-6067150_1920.jpg
    image1:Parcels/street-5375897_1920.jpg
    image2:Parcels/man-5983064_1920.jpg
    image3:Parcels/rice-3466518_1920.jpg
}


*/
