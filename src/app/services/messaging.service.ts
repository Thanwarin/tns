import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class MessagingService {

  apiURL = 'https://fcm.googleapis.com/fcm/send';
  httpOptions = {
    headers: new HttpHeaders({
      // tslint:disable-next-line: object-literal-key-quotes
      'Authorization': 'Bearer AAAAVdWa2OI:APA91bHMnVyBijA-5IiujYBGuRwLgmnAnUS4xdgca_Vw7za9JrJbXvQ4XB0pgkMW-9GV5GgM3PgbkqiKe8Mqwr3-9rxjSXc72XUnocJMw9DI4v76Cknc9WfRFkdQJU7EG8m8uCn68syF',
      'Content-Type': 'application/json'
    })
  // tslint:disable-next-line: semicolon
  }
  pass:string[]=[];
  users$:Observable<any[]>;
  constructor(private http: HttpClient, private angularFirestore: AngularFirestore) {
    this.users$ = this.angularFirestore.collection('users').valueChanges({idField:'ID'});
    this.users$.subscribe(data=>{
      data.forEach(user=>{
        this.pass.push(user.password);
      })
    })
  }
  ngOnInit(): void {
    console.log(this.pass)
  }
  sendMessage(data: any): Promise<any> {
    return this.http.post(this.apiURL, data, this.httpOptions).toPromise().then((response) => response).catch(error => console.log(error));
  }
  async sendeEmail(email:string){

  }
}
