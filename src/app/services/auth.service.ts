import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { UsersModel } from '../@common/model/users';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  verifyEmail = {};
  users$: Observable<any[]>;
  signOut: boolean = false;
  user_Data: UsersModel[] = [];
  profile: any;
  email: any[] = [];
  check_email!: boolean;
  admin: any[] = [];
  nameUser: any[] = [];
  emailUser: any[] = [];
  keyCode: string | null = null;
  constructor(
    private Auth: AngularFireAuth,
    private router: Router,
    private angularFirestore: AngularFirestore,
  ) {
    this.users$ = this.angularFirestore.collection('users').valueChanges({ idField: 'ID' });
    this.users$.subscribe((user) => {
      user.forEach((data) => {
        this.user_Data.push(data)
        const nameUser = data.firstname + data.lastname;
        const emailUser = data.email;
        this.nameUser.push(nameUser);
        this.emailUser.push(emailUser);
        if (data.role === 'admin') {
          this.admin.push(data);
          if (data.key_code !== null) {
            this.keyCode = data.key_code;
          } else if (data.key_code === null) {
            this.keyCode = null;
          }

        }
      })
    })

  }

  singIn(email: string, password: string, remember: boolean) {
    this.Auth.signInWithEmailAndPassword(email, password)
      .then((value) => {
        this.signOut = false;
        this.Auth.idTokenResult.forEach((item) => {
          this.verifyEmail = item?.claims.email_verified;
          if (this.verifyEmail === true) {
            this.user_Data.forEach((users: any) => {
              if (users.email === email) {
                if (atob(`${users.password}`) !== password) {
                  const encode = btoa(password);
                  this.angularFirestore.collection('users').doc(users.ID).update({
                    password: encode
                  })
                }
              }
            })
            sessionStorage.setItem('token', JSON.stringify(item?.token))
            this.admin.forEach((data) => {
              if (email === data.email) {
                this.router.navigateByUrl('/admin');
                sessionStorage.setItem('page', 'admin-home-page');
                sessionStorage.setItem('role', 'admin');
              } else {
                this.router.navigateByUrl('/user');
                sessionStorage.setItem('page', 'home-page');
                sessionStorage.setItem('role', 'user');
              }
            })
          } else if (this.signOut === false) {
            alert('โปรดยืนยันอีเมลเพื่อเข้าใช้งาน');
            firebase.auth().currentUser?.sendEmailVerification();
            location.reload();
          }
        });
      })
      .catch((err) => {
        alert('อีเมลหรือรหัสผ่านไม่ถูกต้อง โปรตรวจสอบอีกครั้ง'),
          console.log(err.message);
      });
  }

  emailSignup(
    email: string,
    password: string,
    department: string,
    firstname: string,
    lastname: string,
    key_code: string
  ) {
    if (this.keyCode !== null) {
      if (key_code === this.keyCode) {
        const checkName = this.nameUser.includes(firstname.toLowerCase() + lastname.toLowerCase());
        const checkEmail = this.emailUser.includes(email.toLowerCase());
        if (checkName === false && checkEmail === false) {
          const encode = btoa(password);
          const email_set = email.toLowerCase();
          this.angularFirestore.collection('users').add({
            firstname: firstname,
            lastname: lastname,
            department: department,
            imageProfile: '',
            agent: {
              agent0: {
                firstname: '',
                lastname: '',
                status_receiver: false,
                email: ''
              },
              agent1: {
                firstname: '',
                lastname: '',
                status_receiver: false,
                email: ''
              },
              agent2: {
                firstname: '',
                lastname: '',
                status_receiver: false,
                email: ''
              },
            },
            role: '',
            email: email_set,
            password: encode,
          }).then(() => {
            alert('ลงทะเบียนเรียบร้อย กรุณาทำการยืนยันอีเมลเพื่อเข้าใช้งาน');
            this.Auth.createUserWithEmailAndPassword(email, password).then((success) => {
              firebase.auth().currentUser?.sendEmailVerification();
              this.router.navigateByUrl('/').then((re) => location.reload());
            })
              .catch((error) => {
                console.log('Something went wrong: ', error);
              });
          })
        } else {
          alert('มีชื่อผู้ใช้งานหรืออีเมลนี้อยู่แล้ว');
        }
      } else {
        alert('รหัสลงทะเบียนไม่ถูกต้อง โปรดตรวจสอบหรือติดต่อเจ้าหน้าที่')
      }
    } else if (this.keyCode === null) {
      alert('รหัสลงทะเบียนไม่ถูกต้อง โปรดตรวจสอบหรือติดต่อเจ้าหน้าที่')
    }
  }

  googleLogin() { //ปิดใช้งาน
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider)
      .then((value) => {
        this.profile = value.additionalUserInfo?.profile;
        this.users$.subscribe((item) => {
          item.forEach((list) => {
            this.email.push(list.email);
          });
          this.check_email = this.email.includes(this.profile.email);
          if (this.check_email === true) {
            if (this.signOut === false) {
              this.Auth.idTokenResult.forEach((item) => {
                sessionStorage.setItem('token', JSON.stringify(item?.token))
              });

              this.router.navigateByUrl('/user');
            }
          } else {
            this.angularFirestore.collection('users').add({
              firstname: this.profile.given_name,
              lastname: this.profile.family_name,
              department: '',
              imageProfile: '',
              agent: {
                agent0: {
                  firstname: '',
                  lastname: '',
                  status_receiver: false
                },
                agent1: {
                  firstname: '',
                  lastname: '',
                  status_receiver: false
                },
                agent2: {
                  firstname: '',
                  lastname: '',
                  status_receiver: false
                },
              },
              role: '',
              email: this.profile.email,
              password: '',
            });
            this.router.navigateByUrl('/');
          }
        });
        return (this.signOut = false);
      })
      .catch((error) => {
        console.log('Something went wrong: ', error);
      });
  }

  logout() {
    this.Auth.signOut().then(() => {
      this.signOut = true;
      sessionStorage.removeItem('token');
      this.router.navigate(['/']).then((re) => {
        location.reload();
      });
    });
  }
  resetPassword(email: string) {
    firebase.auth().sendPasswordResetEmail(email)
      .then(() => {
        alert('ส่งลิ้งค์เพื่อทำการรีเซ็ตรหัสผ่านเรียบร้อย โปรดตรวจสอบอีเมล')
      })
      .catch((err) => {
        alert('ไม่สามารถทำการรีเซ็ตรหัสผ่านได้ โปรดตรวจสอบความถูกต้องของอีเมล')
      });
  }

  private oAuthLogin(provider: any) {
    return this.Auth.signInWithPopup(provider);
  }
}
